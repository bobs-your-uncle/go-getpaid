
Go-GetPaid
==========
A toy payment system.

To start playing with the demo:

Terminal 1
----------
1. $ cd deployments
2. $ ./start.sh
3. ...meditate...

Terminal 2
----------
1. $ cd scripts
2. $ cat README.md
3. $ ./signup.sh alice
4. $ export TOKEN="...token-data-from-the-signup-output..."
5. $ ./add-wallet.sh a1
6. $ ./deposit.sh a1 150
5. $ ./list-wallets.sh
7. $ ./get-balance.sh a1
8. $ ./get-history.sh a1 '?fmt=json&from=2020-09-07T19:00:00Z&op=transfer'
9. ...

Termianl 3
----------
1. $ cd scripts
2. $ ./signup.sh bob
3. $ export TOKEN="...token-data-from-the-signup-output..."
4. $ ./add-wallet.sh b1
5. $ ./deposit.sh b1 333
6. $ ./transfer.sh b1 a1 111
7. $ ./get-balance.sh b1
8. ...

