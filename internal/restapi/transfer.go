package restapi

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"

	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/money"
)

// handleDeposit handles deposit requests
//
// POST /deposit
//
// body params (Form or JSON):
//   - wallet (string)
//   - amount (number)
//
// output (JSON):
//   - change (object):
//	   - tr_id  (number)
//	   - op     (string)
//	   - info   (string)
//	   - token  (string)
//	   - wallet (string)
//	   - amount (number, positive)
//	   - at     (string)
//
func (api *restAPI) handleDeposit(c echo.Context) error {

	var rawToken = api.getRawToken(c)
	if rawToken == "" {
		return fmt.Errorf("there is no JWT token")
	}

	type Params struct {
		Wallet string  `json:"wallet" form:"wallet"`
		Amount float64 `json:"amount" form:"amount"`
	}

	var params Params

	err := c.Bind(&params)
	if err != nil {
		return echo.ErrBadRequest
	}
	if params.Wallet == "" || params.Amount <= 0 {
		return echo.ErrBadRequest
	}

	var ctx = c.Request().Context()
	var username = api.getUsername(c)

	change, err := api.moneySvc.Deposit(ctx, username, rawToken, params.Wallet, params.Amount)
	if err != nil {
		if errors.As(err, &money.ErrNotFound{}) {
			return echo.ErrNotFound
		}
		return fmt.Errorf("failed to deposit funds: %w", err)
	}

	return c.JSONPretty(http.StatusOK, map[string]*JsonChange{
		"change": NewJsonChange(change),
	}, "  ")
}

// handleDeposit handles withdraw requests
//
// POST /withdraw
//
// body params (Form or JSON):
//   - wallet (string)
//   - amount (number)
//
// output (JSON):
//   - change (object):
//	   - tr_id  (number)
//	   - op     (string)
//	   - info   (string)
//	   - token  (string)
//	   - wallet (string)
//	   - amount (number, negative)
//	   - at     (string)
//
func (api *restAPI) handleWithdraw(c echo.Context) error {

	var rawToken = api.getRawToken(c)
	if rawToken == "" {
		return fmt.Errorf("there is no JWT token")
	}

	type Params struct {
		Wallet string  `json:"wallet" form:"wallet"`
		Amount float64 `json:"amount" form:"amount"`
	}

	var params Params

	err := c.Bind(&params)
	if err != nil {
		return echo.ErrBadRequest
	}
	if params.Wallet == "" || params.Amount <= 0 {
		return echo.ErrBadRequest
	}

	var ctx = c.Request().Context()
	var username = api.getUsername(c)

	change, err := api.moneySvc.Withdraw(ctx, username, rawToken, params.Wallet, params.Amount)
	if err != nil {
		if errors.As(err, &money.ErrNotFound{}) {
			return echo.ErrNotFound
		}
		if errors.As(err, &money.ErrNotEnoughFunds{}) {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		return fmt.Errorf("failed to deposit funds: %w", err)
	}

	return c.JSONPretty(http.StatusOK, map[string]*JsonChange{
		"change": NewJsonChange(change),
	}, "  ")
}

// handleTransfer handles transfer requests
//
// POST /transfer
//
// body params (Form or JSON):
//   - from   (string)
//   - to     (string)
//   - amount (number)
//
// output (JSON):
//   - from_change (object):
//	   - tr_id  (number)
//	   - op     (string)
//	   - info   (string)
//	   - token  (string)
//	   - wallet (string)
//	   - amount (number, negative)
//	   - at     (string)
//   - to_change   (object):
//	   - tr_id  (number)
//	   - op     (string)
//	   - info   (string)
//	   - token  (string)
//	   - wallet (string)
//	   - amount (number, positive)
//	   - at     (string)
//
func (api *restAPI) handleTransfer(c echo.Context) error {

	var rawToken = api.getRawToken(c)
	if rawToken == "" {
		return fmt.Errorf("there is no JWT token")
	}

	type Params struct {
		FromWallet string  `json:"from" form:"from"`
		ToWallet   string  `json:"to" form:"to"`
		Amount     float64 `json:"amount" form:"amount"`
	}

	var params Params

	err := c.Bind(&params)
	if err != nil {
		return echo.ErrBadRequest
	}
	if params.FromWallet == "" || params.ToWallet == "" || params.Amount <= 0 {
		return echo.ErrBadRequest
	}

	var ctx = c.Request().Context()
	var username = api.getUsername(c)

	fromChange, toChange, err := api.moneySvc.Transfer(ctx, username, rawToken, params.FromWallet, params.ToWallet, params.Amount)
	if err != nil {
		if errors.As(err, &money.ErrNotFound{}) {
			return echo.ErrNotFound
		}
		if errors.As(err, &money.ErrNotEnoughFunds{}) {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		return fmt.Errorf("failed to deposit funds: %w", err)
	}

	return c.JSONPretty(http.StatusOK, map[string]*JsonChange{
		"from_change": NewJsonChange(fromChange),
		"to_change":   NewJsonChange(toChange),
	}, "  ")
}
