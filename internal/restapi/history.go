package restapi

import (
	"encoding/csv"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"

	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/money"
)

type JsonChange struct {
	TransID   int64     `json:"tr_id"`
	Operation string    `json:"op"`
	Info      string    `json:"info"`
	Token     string    `json:"token"`
	Wallet    string    `json:"wallet"`
	Amount    float64   `json:"amount"`
	CreatedAt time.Time `json:"at"`
}

func NewJsonChange(ch *money.Change) *JsonChange {
	return &JsonChange{
		TransID:   ch.ID,
		Operation: string(ch.Operation),
		Info:      ch.Info,
		Token:     ch.Token,
		Wallet:    ch.Wallet,
		Amount:    ch.Amount,
		CreatedAt: ch.CreatedAt,
	}
}

func NewCsvChange(ch *money.Change) []string {
	return []string{
		ch.CreatedAt.String(),                       // "Timestamp"
		string(ch.Operation),                        // "Operation"
		strconv.FormatFloat(ch.Amount, 'f', -1, 64), // "Amount"
		ch.Wallet,                                // "Wallet"
		strconv.FormatInt(ch.Transaction.ID, 10), // "Transaction ID"
		ch.Info,                                  // "Info"
		ch.Token,                                 // "Token"
	}
}

// handleGetHistory handles balance requests
//
// GET /history/<wallet_name>?fmt=<format>&from=<time>&to=<time>&op=<operation>
//
// query params:
//   - fmt  : output format      ("csv" | "json")
//   - from : starting from time (RFC3339, e.g. "2020-09-08T15:30:00Z")
//   - to   : up to time         (RFC3339, e.g. "2020-09-08T15:45:17Z")
//   - op   : operation          ("deposit" | "withdraw" | "transfer")
//
// output (CSV or JSON):
//   - history (array[object]):
//     [
//		 - tr_id  (number)
//		 - op     (string)
//		 - info   (string)
//		 - token  (string)
//		 - wallet (string)
//		 - amount (number, negative)
//		 - at     (string)
//     ]
//
func (api *restAPI) handleGetHistory(c echo.Context) error {

	var walletName = c.Param("name")
	if walletName == "" {
		return echo.ErrBadRequest
	}

	// query parameters

	type Params struct {
		Format    *string `query:"fmt"`  // "csv" | "json"
		FromTime  *string `query:"from"` // time in the RFC3339 format
		ToTime    *string `query:"to"`   // time in the RFC3339 format
		Operation *string `query:"op"`   // "deposit" | "withdraw" | "transfer"
	}
	var params Params

	err := c.Bind(&params)
	if err != nil {
		return echo.ErrBadRequest
	}

	// param: format
	var format string
	if params.Format != nil {
		format = *params.Format
	}
	switch format {
	case "csv", "json":
		// pass
	case "":
		format = "csv" // default
	default:
		return echo.NewHTTPError(http.StatusBadRequest, "unsupported format: "+format)
	}

	var filters = new(money.ChangeFilters)

	// param: from
	if params.FromTime != nil {
		t, err := time.Parse(time.RFC3339, *params.FromTime)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, "unsupported time format: "+*params.FromTime)
		}
		filters.CreatedAtGE = t
	}

	// param: to
	if params.ToTime != nil {
		t, err := time.Parse(time.RFC3339, *params.ToTime)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, "unsupported time format: "+*params.ToTime)
		}
		filters.CreatedAtLE = t
	}

	// param: op
	if params.Operation != nil {
		switch op := money.Operation(*params.Operation); op {
		case money.OpDeposit, money.OpWithdraw, money.OpTransfer:
			filters.Operation = &op
		default:
			return echo.NewHTTPError(http.StatusBadRequest, "unsupported operation: "+*params.Operation)
		}
	}

	// ask the money service for a history of changes

	var ctx = c.Request().Context()
	var username = api.getUsername(c)

	history, err := api.moneySvc.GetHistory(ctx, username, walletName, filters)
	if err != nil {
		if errors.As(err, &money.ErrNotFound{}) {
			return echo.ErrNotFound
		}
		return fmt.Errorf("failed to get a wallet history: %w", err)
	}

	// export the received data in a requested format

	switch format {
	case "json":
		var changes = make([]*JsonChange, len(history))
		for i, ch := range history {
			changes[i] = NewJsonChange(ch)
		}

		return c.JSONPretty(http.StatusOK, map[string][]*JsonChange{
			"history": changes,
		}, "  ")
	default:
		// streaming CSV data
		var (
			res = c.Response() // Writer
			enc = csv.NewWriter(res)
			hdr = res.Header()
		)
		hdr.Set(echo.HeaderContentType, echo.MIMEOctetStream)
		hdr.Set(echo.HeaderContentDisposition, "attachment; filename="+"result.csv")
		hdr.Set("Content-Transfer-Encoding", "binary")
		hdr.Set("Expires", "0")
		res.WriteHeader(http.StatusOK)

		err := enc.Write([]string{"Timestamp", "Operation", "Amount", "Wallet", "Transaction ID", "Info", "Token"})
		if err != nil {
			return err
		}

		for _, ch := range history {
			err := enc.Write(NewCsvChange(ch))
			if err != nil {
				return err
			}
		}
		enc.Flush()

		return nil
	}
}
