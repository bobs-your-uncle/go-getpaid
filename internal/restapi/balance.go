package restapi

import (
	"errors"
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"

	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/money"
)

// handleGetBalance handles balance requests
//
// GET /balance/<wallet_name>
//
// output (JSON):
//   - balance (number)
//
func (api *restAPI) handleGetBalance(c echo.Context) error {
	var name = c.Param("name")
	if name == "" {
		return echo.ErrBadRequest
	}

	var ctx = c.Request().Context()
	var username = api.getUsername(c)

	wallet, err := api.moneySvc.GetWallet(ctx, username, name)
	if err != nil {
		if errors.As(err, &money.ErrNotFound{}) {
			return echo.ErrNotFound
		}
		return fmt.Errorf("failed to get a wallet: %w", err)
	}

	return c.JSONPretty(http.StatusOK, map[string]float64{
		"balance": wallet.Balance,
	}, "  ")
}
