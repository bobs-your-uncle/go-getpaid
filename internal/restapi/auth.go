package restapi

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"

	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/auth"
)

const (
	sessionExpiresAfter   = 3 * time.Hour
	jwtTokenContextKey    = "jwt_token"
	jwtRawTokenContextKey = "jwt_raw"
)

type AuthParams struct {
	Username string `json:"username" form:"username"`
	Password string `json:"password" form:"password"`
}

// handleSignUp handles sign-up requests
//
// POST /signup
//
// body params (Form or JSON):
//   - username (string)
//   - passowrd (string)
//
// output (JSON):
//   - token (string)
//
func (api *restAPI) handleSignUp(c echo.Context) error {

	var params AuthParams

	if err := c.Bind(&params); err != nil {
		return echo.ErrBadRequest
	}

	// make sure the user doesn't exist
	var ctx = c.Request().Context()
	ok, err := api.authSvc.CheckUsername(ctx, params.Username)
	if err == nil && ok {
		return echo.NewHTTPError(http.StatusBadRequest, "user already exists")
	}
	if err != nil && !errors.As(err, &auth.ErrNotAllowed{}) {
		return fmt.Errorf("failed to check a username: %w", err)
	}

	// register new user
	err = api.authSvc.RegisterUser(ctx, params.Username, params.Password)
	if err != nil {
		return fmt.Errorf("failed to register a user: %w", err)
	}

	// generate and encode the token
	signed, err := api.generateJwtToken(params.Username, sessionExpiresAfter)
	if err != nil {
		return fmt.Errorf("failed to generate a JWT token: %w", err)
	}

	return c.JSONPretty(http.StatusOK, map[string]string{
		"token": signed,
	}, "  ")
}

// handleAuth handles authentication requests
//
// POST /auth
//
// body params (Form or JSON):
//   - username (string)
//   - passowrd (string)
//
// output (JSON):
//   - token (string)
//
func (api *restAPI) handleAuth(c echo.Context) error {

	var params AuthParams

	if err := c.Bind(&params); err != nil {
		return echo.ErrBadRequest
	}

	// check if the user exists an is allowed
	var ctx = c.Request().Context()
	ok, err := api.authSvc.CheckUsernamePassword(ctx, params.Username, params.Password)
	if err != nil {
		if errors.As(err, &auth.ErrNotAllowed{}) {
			return echo.ErrUnauthorized
		}
		return fmt.Errorf("failed to authenticate: %w", err)
	}
	if !ok {
		return echo.ErrUnauthorized
	}

	// generate and encode the token
	signed, err := api.generateJwtToken(params.Username, sessionExpiresAfter)
	if err != nil {
		return fmt.Errorf("failed to generate a JWT token: %w", err)
	}

	return c.JSONPretty(http.StatusOK, map[string]string{
		"token": signed,
	}, "  ")
}

func (api *restAPI) generateJwtToken(username string, ttl time.Duration) (string, error) {
	// generate new JWT token
	token := jwt.New(jwt.SigningMethodHS256)

	// set claims
	claims := token.Claims.(jwt.MapClaims)
	claims["username"] = username
	claims["exp"] = time.Now().Add(ttl).Unix()

	// encode the token
	return token.SignedString(api.jwtKey)
}

// getUsername returns a username from a jwt.Token stored in the Context
// or an empty string if there is no token
//
func (api *restAPI) getUsername(c echo.Context) string {
	token, ok := c.Get(jwtTokenContextKey).(*jwt.Token)
	if !ok || token == nil {
		return ""
	}
	cMap, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return ""
	}
	username, _ := cMap["username"].(string)
	return username
}

// getRawToken returns a raw encoded JWT token stored in the Context
// or an empty string if there is no token
//
func (api *restAPI) getRawToken(c echo.Context) string {
	rawToken, _ := c.Get(jwtRawTokenContextKey).(string)
	return rawToken
}

// authMiddleware is a JWT-based authentication middleware.
// The code here was copied from echo/middleware/jwt.go, stripped down
// and then extended to also consult api.AuthSvc
//
func (api *restAPI) authMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	type jwtExtractor func(echo.Context) (string, error)

	// jwtFromHeader returns a `jwtExtractor` that extracts token from the request header.
	jwtFromHeader := func(header string, authScheme string) jwtExtractor {
		return func(c echo.Context) (string, error) {
			value := c.Request().Header.Get(header)
			l := len(authScheme)
			if len(value) > l+1 && value[:l] == authScheme {
				return value[l+1:], nil
			}
			return "", errors.New("missing or malformed JWT header")
		}
	}

	keyFunc := func(token *jwt.Token) (interface{}, error) {
		// validate the `alg` is what we expect
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected JWT signing method: %v", token.Header["alg"])
		}
		return api.jwtKey, nil
	}

	return func(c echo.Context) error {
		extractor := jwtFromHeader("Authorization", "Bearer")
		rawToken, err := extractor(c)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		token, err := jwt.Parse(rawToken, keyFunc)
		if err == nil && token.Valid {
			// check the username is still allowed
			claims := token.Claims.(jwt.MapClaims)
			username := claims["username"].(string)
			ok, err := api.authSvc.CheckUsername(c.Request().Context(), username)
			if err != nil || !ok {
				if errors.As(err, &auth.ErrNotAllowed{}) {
					return &echo.HTTPError{
						Code:     http.StatusUnauthorized,
						Message:  "invalid username",
						Internal: err,
					}
				}
			}

			// store the token into into the context and call the next handler
			c.Set(jwtTokenContextKey, token)
			c.Set(jwtRawTokenContextKey, rawToken)
			return next(c)
		}
		return &echo.HTTPError{
			Code:     http.StatusUnauthorized,
			Message:  "invalid or expired JWT token",
			Internal: err,
		}
	}
}
