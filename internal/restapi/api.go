package restapi

import (
	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/auth"
	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/money"
)

type restAPI struct {
	authSvc  auth.Service
	moneySvc money.Service
	jwtKey   []byte
}

func NewRestAPI(authSvc auth.Service, moneySvc money.Service, jwtKey []byte) RestAPI {
	return &restAPI{
		authSvc:  authSvc,
		moneySvc: moneySvc,
		jwtKey:   jwtKey,
	}
}

func (api *restAPI) RegisterRoutes(r HttpRouter) {

	r.POST("/signup", api.handleSignUp)
	r.POST("/auth", api.handleAuth)

	restricted := r.Group("", api.authMiddleware)

	restricted.GET("/wallets", api.handleListWallets)
	restricted.POST("/wallets/:name", api.handleCreateWallet)

	restricted.GET("/balance/:name", api.handleGetBalance)
	restricted.GET("/history/:name", api.handleGetHistory)

	restricted.POST("/deposit", api.handleDeposit)
	restricted.POST("/withdraw", api.handleWithdraw)
	restricted.POST("/transfer", api.handleTransfer)
}
