package restapi

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"

	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/money"
)

type JsonWallet struct {
	Name      string    `json:"name"`
	Username  string    `json:"user"`
	CreatedAt time.Time `json:"at"`
	Balance   float64   `json:"balance"`
}

func NewJsonWallet(wallet *money.WalletWithBalance) *JsonWallet {
	return &JsonWallet{
		Name:      wallet.Name,
		Username:  wallet.Username,
		CreatedAt: wallet.CreatedAt,
		Balance:   wallet.Balance,
	}
}

// handleListWallets handles list-wallets requests
//
// GET /wallets
//
// output (JSON):
//   - wallets (array[object]):
//     [
//       - name    (string)
//       - user    (string)
//       - at      (string)
//       - balance (number)
//     ]
//
func (api *restAPI) handleListWallets(c echo.Context) error {

	var username = api.getUsername(c)
	if username == "" {
		return fmt.Errorf("there is no username in the JWT token")
	}

	var ctx = c.Request().Context()

	wallets, err := api.moneySvc.ListWallets(ctx, username)
	if err != nil {
		if errors.As(err, &money.ErrNotFound{}) {
			return echo.ErrNotFound
		}
		return fmt.Errorf("failed to list user wallets: %w", err)
	}

	var jsonWallets = make([]*JsonWallet, len(wallets))
	for i, w := range wallets {
		jsonWallets[i] = NewJsonWallet(w)
	}

	return c.JSONPretty(http.StatusOK, map[string][]*JsonWallet{
		"wallets": jsonWallets,
	}, "  ")
}

// handleCreateWallet handles create-wallet requests
//
// POST /wallets/<wallet_name>
//
// output (JSON):
//   - wallet (object):
//     - name    (string)
//     - user    (string)
//     - at      (string)
//     - balance (number)
//
func (api *restAPI) handleCreateWallet(c echo.Context) error {

	var username = api.getUsername(c)
	if username == "" {
		return fmt.Errorf("there is no username in the JWT token")
	}

	name := c.Param("name")
	if name == "" {
		return echo.ErrBadRequest
	}

	var ctx = c.Request().Context()

	wallet, err := api.moneySvc.AddWallet(ctx, username, name)
	if err != nil {
		if errors.As(err, &money.ErrAlreadyExists{}) {
			return echo.NewHTTPError(http.StatusBadRequest, "wallet already exists")
		}
		return fmt.Errorf("failed to add a wallet: %w", err)
	}

	return c.JSONPretty(http.StatusOK, map[string]*JsonWallet{
		"wallet": NewJsonWallet(wallet),
	}, "  ")
}
