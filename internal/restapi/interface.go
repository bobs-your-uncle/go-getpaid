package restapi

import "github.com/labstack/echo/v4"


type HttpRouter interface {
	GET(route string, handler echo.HandlerFunc, mw ...echo.MiddlewareFunc) *echo.Route
	POST(route string, handler echo.HandlerFunc, mw ...echo.MiddlewareFunc) *echo.Route
	PUT(route string, handler echo.HandlerFunc, mw ...echo.MiddlewareFunc) *echo.Route
	DELETE(route string, handler echo.HandlerFunc, mw ...echo.MiddlewareFunc) *echo.Route

	Use(...echo.MiddlewareFunc)
	Group(prefix string, mw ...echo.MiddlewareFunc) *echo.Group
}

type RestAPI interface {
	RegisterRoutes(HttpRouter)
}

