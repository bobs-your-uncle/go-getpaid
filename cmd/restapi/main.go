package main

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	_ "github.com/jackc/pgx/v4/stdlib" // register "pgx" sql driver
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"

	"bitbucket.org/bobs-your-uncle/go-getpaid/internal/restapi"
	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/auth"
	authRepo "bitbucket.org/bobs-your-uncle/go-getpaid/pkg/auth/repo/kv"
	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/kv/redis"
	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/secret"
	secretEnv "bitbucket.org/bobs-your-uncle/go-getpaid/pkg/secret/env"
	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/money"
	moneyRepo "bitbucket.org/bobs-your-uncle/go-getpaid/pkg/money/repo/postgres"
)

const (
	shutdownTimeout = 10 * time.Second

	jwtKeySecretName = "JWT_KEY"

	redisAddressSecretName  = "REDIS_ADDRESS"
	redisPasswordSecretName = "REDIS_PASSWORD"
	redisTlsSecretName      = "REDIS_TLS"

	postgresHostSecretName     = "POSTGRES_HOST"
	postgresDbSecretName       = "POSTGRES_DB"
	postgresUserSecretName     = "POSTGRES_USER"
	postgresPasswordSecretName = "POSTGRES_PASSWORD"
)

// newEchoServer creates an Echo HTTP server instance and sets up necessary middleware
func newEchoServer() *echo.Echo {
	// Echo instance
	e := echo.New()

	e.Logger.SetLevel(log.INFO)
	e.Logger.SetHeader("${level} ${time_rfc3339}")

	// setup common middleware
	e.Use(
		middleware.LoggerWithConfig(middleware.LoggerConfig{
			Format:           `${time_custom} [${status}] ${method} ${uri} error="${error}"` + "\n",
			CustomTimeFormat: "2006-01-02 15:04:05",
		}),
		middleware.Recover(),
		middleware.Secure(),
		middleware.RequestID(),
		middleware.BodyLimit("1K"), // we don't expect anything large
		middleware.Gzip(),
	)

	return e
}

// buildAuthService assembles an authentication service taking parameters
// from a supplied secret.Source
//
func buildAuthService(ctx context.Context, secrets secret.Source) (auth.Service, error) {
	address, err := secrets.Peep(ctx, redisAddressSecretName)
	if errors.As(err, &secret.ErrNotFound{}) {
		address = "localhost:6379"
		err = nil
	}
	if err != nil {
		return nil, fmt.Errorf("failed to peep a secret %v: %w", redisAddressSecretName, err)
	}

	password, err := secrets.Peep(ctx, redisPasswordSecretName)
	if errors.As(err, &secret.ErrNotFound{}) {
		password = ""
		err = nil
	}
	if err != nil {
		return nil, fmt.Errorf("failed to peep a secret %v: %w", redisPasswordSecretName, err)
	}

	tls, err := secrets.Peep(ctx, redisTlsSecretName)
	if errors.As(err, &secret.ErrNotFound{}) {
		tls = ""
		err = nil
	}
	if err != nil {
		return nil, fmt.Errorf("failed to peep a secret %v: %w", redisTlsSecretName, err)
	}

	poolMaker := redis.NewPoolMaker(&redis.Config{
		Address:        address,
		Password:       password,
		TLS:            tls != "",
		MaxIdle:        50,
		IdleTimeout:    10 * time.Minute,
		ConnectTimeout: 15 * time.Second,
	})
	pool, err := poolMaker(address)
	if err != nil {
		return nil, fmt.Errorf("failed to create a redis.Pool: %w", err)
	}

	kv := redis.NewKV(pool)

	authRepo := authRepo.NewRepository(kv)
	authSvc := auth.NewService(authRepo)

	return authSvc, nil
}

// buildMoneyService assembles a money service taking parameters from a supplied secret.Source
//
func buildMoneyService(ctx context.Context, secrets secret.Source) (money.Service, error) {

	pgHost, err := secrets.Peep(ctx, postgresHostSecretName)
	if errors.As(err, &secret.ErrNotFound{}) {
		pgHost = "localhost"
		err = nil
	}
	if err != nil {
		return nil, fmt.Errorf("failed to peep a secret %v: %w", postgresPasswordSecretName, err)
	}

	pgUser, err := secrets.Peep(ctx, postgresUserSecretName)
	if err != nil {
		return nil, fmt.Errorf("failed to peep a secret %v: %w", postgresPasswordSecretName, err)
	}

	pgPassword, err := secrets.Peep(ctx, postgresPasswordSecretName)
	if errors.As(err, &secret.ErrNotFound{}) {
		pgPassword = ""
		err = nil
	}
	if err != nil {
		return nil, fmt.Errorf("failed to peep a secret %v: %w", postgresPasswordSecretName, err)
	}

	pgDatabase, err := secrets.Peep(ctx, postgresDbSecretName)
	if err != nil {
		return nil, fmt.Errorf("failed to peep a secret %v: %w", postgresPasswordSecretName, err)
	}

	pgUrl := fmt.Sprintf("postgresql://%v:%v@%v/%v", pgUser, pgPassword, pgHost, pgDatabase)
	db, err := sql.Open("pgx", pgUrl)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to a database: %w", err)
	}

	repo := moneyRepo.NewRepository(db)
	svc := money.NewService(repo)

	return svc, nil
}

func main() {
	// a context for initialization stage
	initCtx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// create new Echo server
	server := newEchoServer()

	// get a JWT key
	secretrc := secretEnv.NewSource()

	jwtKey, err := secretrc.Peep(initCtx, jwtKeySecretName)
	if err != nil {
		server.Logger.Fatalf("failed to get a JWT key secret: %v", err)
	}

	// build an authentication service
	authSvc, err := buildAuthService(initCtx, secretrc)
	if err != nil {
		server.Logger.Fatalf("failed to build an authentication service: %v", err)
	}

	// build a money service
	moneySvc, err := buildMoneyService(initCtx, secretrc)
	if err != nil {
		server.Logger.Fatalf("failed to build an authentication service: %v", err)
	}

	// setup an API controller and register the routes
	api := restapi.NewRestAPI(authSvc, moneySvc, []byte(jwtKey))
	api.RegisterRoutes(server)

	// start server
	go func() {
		if err := server.Start(":8020"); err != nil {
			server.Logger.Info("shutting down the server")
		}
	}()

	// wait for an OS signal to gracefully shutdown the server with a reasonable timeout
	quit := make(chan os.Signal, 4)
	signal.Notify(quit, os.Interrupt, os.Signal(syscall.SIGTERM))
	sig := <-quit // block waiting
	server.Logger.Warnf("received an OS signal %v, shutting down", sig)
	ctx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		server.Logger.Fatal(err)
	}
}
