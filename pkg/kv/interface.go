package kv

// KV interface describes a simple key-value store
type KV interface {
	// Get returns current value of the key. If there is no such key returns ErrNotFound{}
	Get(key string) (string, error) 

	// Set updates the key's value according to the given UpdatePolicy.
	// The key's expiration gets updated if >0, left the same ==0 or reset if <0.
	Set(key Key, value string, upd UpdatePolicy) (bool, error)
	
	// Delete removes values from the given keys
	Delete(keys ...string) (int, error)
}

// AtomicKV interface describes a key-value store that also provides
// advanced atomic primitives
type AtomicKV interface {
	KV

	// GetOrSet returns either an existing value of the key or sets a new one.
	// Returns current value.
	GetOrSet(key Key, newVal string) (cur string, err error)

	// Replace sets a new value for the key and returns its previous value.
	// The key's expiration gets updated if >0, left the same ==0 or reset if <0.
	Replace(key Key, newVal string) (prev string, err error)

	// ReplaceIf checks if current value of the key is one of the given values
	// and if so replaces it with a new value. When replacing is done
	// the key's expiration gets updated if >0, left the same ==0 or reset if <0.
	// Returns a boolean flag indicating the value was reset and the previous value.
	ReplaceIf(key Key, newVal string, oldValues ...string) (ok bool, prev string, err error)

	// DeleteIf deletes a key if its value is one of the given values.
	// Returns a boolean flag indicating if the key was deleted and the previous value.
	DeleteIf(key string, oldValues ...string) (ok bool, prev string, err error) 
}

