package kv

// UpdatePolicy encodes possible update stategies
type UpdatePolicy byte

const (
	UpdateExisting UpdatePolicy = iota // update existing members and add new members
	UpdateNothing                      // ignore existing members and add new members
	UpdateOnly                         // update existing members and ignore new members
)

//go:generate enumer -type=UpdatePolicy -text -json -transform=camelcase -output=enum_update_policy_gen.go
