package kv

import (
	"fmt"
	"strings"
	"time"
)

const (
	MaxScore = 9007199254740992  //  1<<53 (Redis limitation)
	MinScore = -9007199254740992 // -1<<53 (Redis limitation)
)

type Key struct {
	Name       string
	Expiration time.Duration
}

type ScoredValue struct {
	Value string
	Score int64
}

func NewKey(name string) Key {
	return Key{Name: name}
}

func (k Key) String() string {
	var b strings.Builder
	b.WriteString(fmt.Sprintf("Key %#v", k.Name))
	if k.Expiration > 0 {
		b.WriteString(fmt.Sprintf(" (expires after %v)", k.Expiration))
	}
	return b.String()
}

// WithExpiration returns a copy of the given Key with a specific expiration time set
func (k Key) WithExpiration(exp time.Duration) Key {
	k.Expiration = exp
	return k // a copy
}
