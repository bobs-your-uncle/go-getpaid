package kv

import (
	"fmt"
)

// ErrNotFound is an error returned when a requested key doesn't exist
type ErrNotFound struct {
	Key string
}

// Error implements the error interface for ErrNotFound.
func (e ErrNotFound) Error() string {
	return fmt.Sprintf("Key %#v doesn't exist", e.Key)
}

