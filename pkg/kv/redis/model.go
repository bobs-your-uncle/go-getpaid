// package redis implements the kv.KV interface using a single node Redis server
package redis

import (
	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/kv"
)

type redisKV struct {
	pool Pool
}

func NewKV(pool Pool) kv.AtomicKV {
	return &redisKV{
		pool: pool,
	}
}
