package redis

import (
	"fmt"
	"time"

	"github.com/gomodule/redigo/redis"
)

type Pool interface {
	Get() redis.Conn
}

func NewPoolMaker(cfg *Config) func(string, ...redis.DialOption) (*redis.Pool, error) {
	var (
		defaultAddr = cfg.Address
		defaultOpts = []redis.DialOption{redis.DialConnectTimeout(cfg.ConnectTimeout)}
	)
	if cfg.TLS {
		defaultOpts = append(defaultOpts, redis.DialUseTLS(true), redis.DialTLSSkipVerify(true))
	}
	if cfg.Password != "" {
		defaultOpts = append(defaultOpts, redis.DialPassword(cfg.Password))
	}
	return func(addr string, opts ...redis.DialOption) (*redis.Pool, error) {
		return &redis.Pool{
			// Maximum number of idle connections in the pool.
			// No pooling will happen without this set.
			MaxIdle: cfg.MaxIdle,

			// Purge connections that have been idle for this long.
			// This has some overhead, as it is trimmed on each connection, but it
			// is the only way to eventually shrink the connection count when there are
			// bursts of activity, and MaxActive is 0.
			IdleTimeout: cfg.IdleTimeout,

			// This includes both idle connections in the pool and active connections in use.
			// 0 means no limit on the number of active connections.
			MaxActive: 0,

			// We can never wait for a connection to be returned from the pool,
			// because this is very easy to accidentaily introduce deadlocks with nested calls.
			Wait: false,

			// Provide a function to create a new connection, when there are no healthy idle connections
			// in the pool.
			Dial: func() (redis.Conn, error) {
				if addr == "" {
					addr = defaultAddr
				}
				if opts == nil {
					opts = defaultOpts
				}

				c, err := redis.Dial("tcp", addr, opts...)
				if err != nil {
					return nil, fmt.Errorf("Redis: failed to connect to %v: %w", addr, err)
				}

				return c, err
			},
			// Checks health of idle connection before allowing use again.
			// If this returns an error, the connection is closed
			TestOnBorrow: func(c redis.Conn, t time.Time) error {
				// Limit how often we run this check, we don't want an extra ping along with every redis call
				if time.Since(t) < 1*time.Minute {
					return nil
				}

				_, err := c.Do("PING")
				if err != nil {
					return fmt.Errorf("unable to ping Redis while checking an idle connection: %w", err)
				}
				return nil
			},
		}, nil
	}
}
