package redis

import (
	"time"
)

type Config struct {
	Address        string
	Cluster        bool
	TLS            bool // encrypted connection
	Password       string
	ConnectTimeout time.Duration
	IdleTimeout    time.Duration
	MaxIdle        int
}

