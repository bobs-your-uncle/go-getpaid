package redis

import (
	"errors"
	"fmt"
	"time"

	"github.com/gomodule/redigo/redis"

	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/kv"
)

// -- kv.KV implementation --

func (c *redisKV) Get(key string) (string, error) {
	conn := c.pool.Get()
	if err := conn.Err(); err != nil {
		return "", err
	}
	defer conn.Close()

	res, err := conn.Do("GET", key)
	val, err := redis.String(res, err)
	if err != nil {
		if errors.Is(err, redis.ErrNil) {
			return "", kv.ErrNotFound{Key: key}
		}
		return "", fmt.Errorf("failed to GET a key %#v: %w", key, err)
	}

	return val, nil
}

func getKeyExpirationArgs(key kv.Key) []interface{} {
	switch {
	case key.Expiration == 0:
		return []interface{}{"KEEPTTL"}
	case key.Expiration > 0:
		return []interface{}{"PX", int64(key.Expiration / time.Millisecond)}
	}
	return []interface{}{}
}
func getUpdateArgs(upd kv.UpdatePolicy) []interface{} {
	switch upd {
	case kv.UpdateExisting:
		// pass
	case kv.UpdateNothing:
		return []interface{}{"NX"}
	case kv.UpdateOnly:
		return []interface{}{"XX"}
	}
	return []interface{}{}
}
func prepareSetArgs(key kv.Key, val string, upd kv.UpdatePolicy) []interface{} {
	var args = []interface{}{key.Name, val}
	args = append(args, getKeyExpirationArgs(key)...)
	return append(args, getUpdateArgs(upd)...)
}

var scriptSet = redis.NewScript(1, `
	local args = ARGV
	-- emulate KEEPTTL for redis <6.0
	if #args >= 2 and args[2] == "KEEPTTL" then
		local ttl = redis.call("PTTL", KEYS[1])
		if ttl > 0 then
			args = {args[1], "PX", ttl, unpack(args, 3)}
		else
			args = {args[1], unpack(args, 3)}
		end
	end
	return redis.call("SET", KEYS[1], unpack(args))
`)

func (c *redisKV) Set(key kv.Key, val string, upd kv.UpdatePolicy) (bool, error) {
	conn := c.pool.Get()
	if err := conn.Err(); err != nil {
		return false, err
	}
	defer conn.Close()

	var args = prepareSetArgs(key, val, upd)

	res, err := scriptSet.Do(conn, args...)
	out, err := redis.String(res, err)
	if err != nil {
		if errors.Is(err, redis.ErrNil) {
			return false, nil
		}
		return false, fmt.Errorf("failed to execute the Set script for %v: %w", key, err)
	}
	return out == "OK", nil
}

func (c *redisKV) Delete(keys ...string) (int, error) {
	if len(keys) == 0 {
		return 0, nil
	}

	conn := c.pool.Get()
	if err := conn.Err(); err != nil {
		return 0, err
	}
	defer conn.Close()

	var args = make([]interface{}, len(keys))
	for i, key := range keys {
		args[i] = key
	}

	res, err := conn.Do("DEL", args...)
	if err != nil {
		return 0, fmt.Errorf("failed to delete keys %#v: %w", keys, err)
	}

	num, _ := redis.Int(res, nil)

	return num, nil
}

// -- kv.AtomicKV implementation --

var scriptGetOrSet = redis.NewScript(1, `
    local val = redis.call("GET", KEYS[1])
	if not val then
		val = ARGV[1]
		local args = ARGV
		-- emulate KEEPTTL for redis <6.0
		if #args >= 2 and args[2] == "KEEPTTL" then
			local ttl = redis.call("PTTL", KEYS[1])
			if ttl > 0 then
				args = {args[1], "PX", ttl, unpack(args, 3)}
			else
				args = {args[1], unpack(args, 3)}
			end
		end
		redis.call("SET", KEYS[1], unpack(args))
	end
	return val
`)

func (c *redisKV) GetOrSet(key kv.Key, val string) (string, error) {
	conn := c.pool.Get()
	if err := conn.Err(); err != nil {
		return "", err
	}
	defer conn.Close()

	var args = prepareSetArgs(key, val, kv.UpdateExisting)

	res, err := scriptGetOrSet.Do(conn, args...)
	out, err := redis.String(res, err)
	if err != nil {
		return "", fmt.Errorf("failed to execute the GetOrSet script for %v: %w", key, err)
	}

	return out, nil
}

var scriptReplace = redis.NewScript(1, `
    local val  = redis.call("GET", KEYS[1])
	local args = ARGV
	-- emulate KEEPTTL for redis <6.0
	if #args >= 2 and args[2] == "KEEPTTL" then
		local ttl = redis.call("PTTL", KEYS[1])
		if ttl > 0 then
			args = {args[1], "PX", ttl, unpack(args, 3)}
		else
			args = {args[1], unpack(args, 3)}
		end
	end
	redis.call("SET", KEYS[1], unpack(args))
	return val
`)

func (c *redisKV) Replace(key kv.Key, val string) (string, error) {
	conn := c.pool.Get()
	if err := conn.Err(); err != nil {
		return "", err
	}
	defer conn.Close()

	var args = prepareSetArgs(key, val, kv.UpdateExisting)

	res, err := scriptReplace.Do(conn, args...)
	out, err := redis.String(res, err)
	if err != nil {
		if errors.Is(err, redis.ErrNil) {
			return "", nil
		}
		return "", fmt.Errorf("failed to execute the Replace script for a %v: %w", key, err)
	}

	return out, nil
}

var scriptReplaceIf = redis.NewScript(1, `
    local actual = redis.call("GET", KEYS[1])
	if not actual then
		actual = ""
	end
	local nReplaced = 0
	local nExpected = ARGV[1]
	local expected  = {unpack(ARGV, 2, 1+nExpected)}
	for _, value in pairs(expected) do
		if actual == value then
			local args = {unpack(ARGV, 2+nExpected)}
			-- emulate KEEPTTL for redis <6.0
			if #args >= 2 and args[2] == "KEEPTTL" then
				local ttl = redis.call("PTTL", KEYS[1])
				if ttl > 0 then
					args = {args[1], "PX", ttl, unpack(args, 3)}
				else
					args = {args[1], unpack(args, 3)}
				end
			end
			redis.call("SET", KEYS[1], unpack(args))
			nReplaced = 1
			break
		end
	end
	return {nReplaced, actual}
`)

func (c *redisKV) ReplaceIf(key kv.Key, newVal string, oldValues ...string) (bool, string, error) {
	conn := c.pool.Get()
	if err := conn.Err(); err != nil {
		return false, "", err
	}
	defer conn.Close()

	if oldValues == nil {
		oldValues = []string{} // there should be no nils in lua tables
	}

	var args = make([]interface{}, 0, 1+1+len(oldValues)+3) // {"key", numOld, "old1", ..., "new" [, "PX" expire]}
	args = append(args, key.Name, len(oldValues))
	for _, value := range oldValues {
		args = append(args, value)
	}
	args = append(args, newVal)
	args = append(args, getKeyExpirationArgs(key)...)

	res, err := scriptReplaceIf.Do(conn, args...)
	out, err := redis.Values(res, err)
	if err != nil {
		return false, "", fmt.Errorf("failed to execute the ReplaceIf script for a key %#v: %w", key, err)
	}
	if len(out) != 2 {
		return false, "", fmt.Errorf("unexpected result of the ReplaceIf script for a key %#v: %v", key, out)
	}
	num, err := redis.Int(out[0], err)
	if err != nil {
		return false, "", fmt.Errorf("unexpected result[0] %#v of the ReplaceIf script for a key %#v: %w", out[0], key, err)
	}
	prev, err := redis.String(out[1], err)
	if err != nil {
		return false, "", fmt.Errorf("unexpected result[1] %#v of the ReplaceIf script for a key %#v: %w", out[1], key, err)
	}

	return num > 0, prev, nil
}

var scriptDeleteIf = redis.NewScript(1, `
    local actual = redis.call("GET", KEYS[1])
	local num = 0
	if actual then
		for _, expected in pairs(ARGV) do
			if actual == expected then
				redis.call("DEL", KEYS[1])
				num = 1
				break
			end
		end
	else
		actual = ""
	end
	return {num, actual}
`)

func (c *redisKV) DeleteIf(key string, values ...string) (bool, string, error) {
	conn := c.pool.Get()
	if err := conn.Err(); err != nil {
		return false, "", err
	}
	defer conn.Close()

	var args = make([]interface{}, 0, 1+len(values))
	args = append(args, key)
	for _, value := range values {
		args = append(args, value)
	}

	res, err := scriptDeleteIf.Do(conn, args...)
	out, err := redis.Values(res, err)
	if err != nil {
		return false, "", fmt.Errorf("failed to execute the DeleteIf script for a key %#v: %w", key, err)
	}
	if len(out) != 2 {
		return false, "", fmt.Errorf("unexpected result of the DeleteIf script for a key %#v: %v", key, out)
	}
	num, err := redis.Int(out[0], err)
	if err != nil {
		return false, "", fmt.Errorf("unexpected result[0] %#v of the DeleteIf script for a key %#v: %w", out[0], key, err)
	}
	prev, err := redis.String(out[1], err)
	if err != nil {
		return false, "", fmt.Errorf("unexpected result[1] %#v of the DeleteIf script for a key %#v: %w", out[1], key, err)
	}

	return num > 0, prev, nil
}
