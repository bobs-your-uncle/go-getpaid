// env contains an implementation of the secrets.Source interface
// that looks int OS environment variables for the requested secrets
package env

import (
	"context"
	"os"
	"strings"

	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/secret"
)

type source struct {
	overrideMap map[string]string
}

// NewSource creates a new secrets.Source that looks into environment variables
// for secrets. Optional `overrides` parameters can be used to override certain
// "key=value" pairs in the environment.
//
func NewSource(overrides ...string) secret.Source {
	var oMap = make(map[string]string, len(overrides))
	for _, kv := range overrides {
		var (
			parts = strings.SplitN(kv, "=", 2)
			key   = parts[0]
			value string
		)
		if len(parts) == 2 {
			value = parts[1]
		}
		oMap[key] = value
	}

	return &source{
		overrideMap: oMap,
	}
}

func (src *source) Peep(_ context.Context, name string) (string, error) {
	value, ok := src.overrideMap[name]
	if !ok {
		value, ok = os.LookupEnv(name)
	}
	if !ok {
		return "", secret.ErrNotFound{Name: name, Source: "env.source{...}"}
	}
	return value, nil
}
