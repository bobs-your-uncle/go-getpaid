package secret

import "fmt"

// ErrNotFound - an error that is returned when a requested secret is not found
type ErrNotFound struct {
	Name   string
	Source string
}

func (err ErrNotFound) Error() string {
	return fmt.Sprintf("secret %#v is not found in %v", err.Name, err.Source)
}
