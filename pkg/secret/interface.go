package secret

import "context"

// Source - an abstract source of secrets
type Source interface {
	// Peep a secret
	Peep(ctx context.Context, name string) (string, error)
}
