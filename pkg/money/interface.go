package money

import (
	"context"
	"time"
)

// Service - an abstract money transfer service
type Service interface {
	// ListWallets returns a list of wallets of the given user
	ListWallets(ctx context.Context, username string) ([]*WalletWithBalance, error)

	// GetWallet returns the given wallet by its name
	GetWallet(ctx context.Context, username, wallet string) (*WalletWithBalance, error)

	// AddWallet creates and stores new named wallet for the given user
	AddWallet(ctx context.Context, username, wallet string) (*WalletWithBalance, error)

	// Transfer creates and stores new "transfer" transaction with corresponding wallet changes
	Transfer(ctx context.Context, username, token, fromWallet, toWallet string, amount float64) (*Change, *Change, error)

	// Deposit creates and stores new "deposit" transaction with a corresponding wallet change
	Deposit(ctx context.Context, username, token, wallet string, amount float64) (*Change, error)

	// Withdraw creates and stores new "withdraw" transaction with a corresponding wallet change
	Withdraw(ctx context.Context, username, token, wallet string, amount float64) (*Change, error)

	// GetHistory returns a list of filtered wallet changes for the given wallet
	GetHistory(ctx context.Context, username, wallet string, filters *ChangeFilters) ([]*Change, error)
}

// Repository - an abstract repository for the money transfer service
type Repository interface {
	// Isolated runs `fn` in an isolated transaction that might repeat several times
	Isolated(ctx context.Context, maxAttempts int, fn func(Repository) error) error

	// ListWallets returns a list of wallets of the given user
	ListWallets(ctx context.Context, username string) ([]*Wallet, error)

	// GetWallet returns the given wallet by its name
	GetWallet(ctx context.Context, wallet string) (*Wallet, error)

	// AddWallet creates and stores new named wallet for the given user
	AddWallet(ctx context.Context, username, wallet string, at time.Time) (*Wallet, error)

	// AddTransaction creates and stores new transaction
	AddTransaction(ctx context.Context, op Operation, info, token string, at time.Time) (*Transaction, error)

	// AddChange creates and stores new wallet change
	AddChange(ctx context.Context, trans *Transaction, wallet string, amount float64, at time.Time) (*Change, error)

	// SumChanges adds up the `amount` field of filtered wallet changes
	SumChanges(ctx context.Context, filters *ChangeFilters) (float64, error)

	// ListChanges returns a list of filtered wallet changes
	ListChanges(ctx context.Context, filters *ChangeFilters) ([]*Change, error)
}

//go:generate mockery --name=Repository --inpackage
