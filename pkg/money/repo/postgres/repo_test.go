package postgres

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"

	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/money"
)

func TestIsolatedRollsBackOnError(t *testing.T) {
	var (
		db, dbMock, _ = sqlmock.New()
		r             = NewRepository(db)
		e             = errors.New("TEST ERROR")
		ctx, cancel   = context.WithTimeout(context.Background(), 10*time.Second)
	)
	defer func() {_ = db.Close()}()
	defer cancel()

	dbMock.ExpectBegin()
	dbMock.ExpectRollback()

	// --- business logic ---
	err := r.Isolated(ctx, 3, func(_ money.Repository) error { return e })
	// ----------------------

	assert.EqualError(t, err, e.Error())
	assert.NoError(t, dbMock.ExpectationsWereMet())
}

func TestIsolatedRollsBackOnPanic(t *testing.T) {
	var (
		db, dbMock, _ = sqlmock.New()
		r             = NewRepository(db)
		ctx, cancel   = context.WithTimeout(context.Background(), 10*time.Second)
	)
	defer func() {_ = db.Close()}()
	defer cancel()

	dbMock.ExpectBegin()
	dbMock.ExpectRollback()

	// --- business logic ---
	var err error
	var panicFn = func() {
		err = r.Isolated(ctx, 3, func(_ money.Repository) error { panic("Achtung!") })
	}
	assert.PanicsWithValue(t, "Achtung!", panicFn)
	// ----------------------

	assert.NoError(t, err)
	assert.NoError(t, dbMock.ExpectationsWereMet())
}

func TestIsolatedCommits(t *testing.T) {
	var (
		db, dbMock, _ = sqlmock.New()
		r             = NewRepository(db)
		ctx, cancel   = context.WithTimeout(context.Background(), 10*time.Second)
	)
	defer func() {_ = db.Close()}()
	defer cancel()

	dbMock.ExpectBegin()
	dbMock.ExpectCommit()

	// --- business logic ---
	err := r.Isolated(ctx, 3, func(_ money.Repository) error { return nil })
	// ----------------------

	assert.NoError(t, err)
	assert.NoError(t, dbMock.ExpectationsWereMet())
}

func TestIsolatedHandlesCommitError(t *testing.T) {
	var (
		db, dbMock, _ = sqlmock.New()
		r             = NewRepository(db)
		e = errors.New("TEST ERROR")
		ctx, cancel   = context.WithTimeout(context.Background(), 10*time.Second)
	)
	defer func() {_ = db.Close()}()
	defer cancel()

	dbMock.ExpectBegin()
	dbMock.ExpectCommit().WillReturnError(e)

	// --- business logic ---
	err := r.Isolated(ctx, 3, func(_ money.Repository) error { return nil })
	// ----------------------

	assert.EqualError(t, err, e.Error())
	assert.NoError(t, dbMock.ExpectationsWereMet())
}

func TestIsolatedRetriesOnSerializeError(t *testing.T) {
	var (
		db, dbMock, _ = sqlmock.New()
		r             = NewRepository(db)
		e = errors.New("ERROR: could not serialize access due to concurrent update")
		ctx, cancel   = context.WithTimeout(context.Background(), 10*time.Second)
	)
	defer func() {_ = db.Close()}()
	defer cancel()

	// 1st attempt
	dbMock.ExpectBegin()
	dbMock.ExpectCommit().WillReturnError(e)

	// 2nd attempt
	dbMock.ExpectBegin()
	dbMock.ExpectCommit()

	// --- business logic ---
	err := r.Isolated(ctx, 3, func(_ money.Repository) error { return nil })
	// ----------------------

	assert.NoError(t, err)
	assert.NoError(t, dbMock.ExpectationsWereMet())
}
