package postgres

import (
	"context"
	"database/sql"
	"strings"

	"github.com/doug-martin/goqu/v9"
	_ "github.com/doug-martin/goqu/v9/dialect/postgres" // register the dialect

	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/money"
)

// repo - an implementation of the money.Repository using PostgreSQL
type repo struct {
	gq *goqu.Database
	tx *goqu.TxDatabase
}

func NewRepository(db goqu.SQLDatabase) money.Repository {
	return &repo{
		gq: goqu.New("postgres", db),
		tx: nil,
	}
}

func (r *repo) Isolated(ctx context.Context, maxAttempts int, fn func(money.Repository) error) (err error) {

	var (
		tx      *goqu.TxDatabase
		success = false
	)

	defer func() {
		// we don't use recover to keep the panic going,
		// instead we use a boolean flag `success` – false means we
		// jumped here in a panic during the fn() execution
		if !success && tx != nil {
			txErr := tx.Rollback()
			if txErr != nil {
				err = txErr
			}
		}
	}()

	for attempt := 1; attempt <= maxAttempts; attempt++ {
		tx, err = r.gq.BeginTx(ctx, &sql.TxOptions{
			Isolation: sql.LevelSerializable, // IMPORTANT: we want the strongest level of isolation
		})
		if err != nil {
			return err // defer would rollback
		}

		var txRepo = &repo{gq: r.gq, tx: tx}
		err = fn(txRepo) // might panic – defer would rollback

		if err != nil {
			return err // defer would rollback
		}

		// try to commit
		if err = tx.Commit(); err != nil {

			_ = tx.Rollback() // rollback just to be on the safe side

			if strings.Contains(err.Error(), "could not serialize access") {
				// this type of error suggests there was a serialization anomaly in the db,
				// i.e. some other connection has modified the same rows at the same time.
				// which means there is a good chance we can try to fix things by simply
				// repeating the operation.
				//
				continue
			}
		}

		break // don't retry, don't rollback
	}

	success = true // disarm the deferred rollback

	return err
}

func (r *repo) from(args ...interface{}) *goqu.SelectDataset {
	if r.tx != nil {
		return r.tx.From(args...)
	}
	return r.gq.From(args...)
}
func (r *repo) insert(table interface{}) *goqu.InsertDataset {
	if r.tx != nil {
		return r.tx.Insert(table)
	}
	return r.gq.Insert(table)
}
