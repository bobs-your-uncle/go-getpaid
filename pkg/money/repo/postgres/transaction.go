package postgres

import (
	"context"
	"time"

	"github.com/doug-martin/goqu/v9"

	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/money"
)

const transactionTable = "transactions"

type transactionRow struct {
	ID        int64     `db:"id" goqu:"skipinsert,skipupdate"`
	Operation string    `db:"op" goqu:"skipupdate"`
	Info      string    `db:"info" goqu:"skipupdate"`
	Token     string    `db:"token" goqu:"skipupdate"`
	CreatedAt time.Time `db:"created_at" goqu:"skipupdate"`
}

func (row *transactionRow) ToTransaction() *money.Transaction {
	return &money.Transaction{
		ID:        row.ID,
		Operation: money.Operation(row.Operation),
		Info:      row.Info,
		Token:     row.Token,
		CreatedAt: row.CreatedAt,
	}
}

func (r *repo) AddTransaction(ctx context.Context, op money.Operation, info, token string, at time.Time) (*money.Transaction, error) {
	var row = &transactionRow{
		Operation: string(op),
		Info:      info,
		Token:     token,
		CreatedAt: at,
	}
	insert := r.insert(transactionTable).Rows(row).Returning(goqu.I("id")).Executor()
	var id int64
	_, err := insert.ScanValContext(ctx, &id)
	if err != nil {
		return nil, err
	}
	row.ID = id

	return row.ToTransaction(), nil
}
