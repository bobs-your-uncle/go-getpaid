package postgres

import (
	"context"
	"time"

	"github.com/doug-martin/goqu/v9"

	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/money"
)

const walletTable = "wallets"

type walletRow struct {
	ID        int64     `db:"id" goqu:"skipinsert,skipupdate"`
	Name      string    `db:"name" goqu:"skipupdate"`
	Username  string    `db:"username" goqu:"skipupdate"`
	CreatedAt time.Time `db:"created_at" goqu:"skipupdate"`
}

func (row *walletRow) ToWallet() *money.Wallet {
	return &money.Wallet{
		Name:      row.Name,
		Username:  row.Username,
		CreatedAt: row.CreatedAt,
	}
}

func (r *repo) ListWallets(ctx context.Context, username string) ([]*money.Wallet, error) {
	var rows []walletRow
	err := r.from(walletTable).
		Where(goqu.I("username").Eq(username)).
		ScanStructsContext(ctx, &rows)
	if err != nil {
		return nil, err
	}

	wallets := make([]*money.Wallet, len(rows))
	for i, row := range rows {
		wallets[i] = row.ToWallet()
	}

	return wallets, nil
}

func (r *repo) getWallet(ctx context.Context, wallet string) (*walletRow, error) {
	var row walletRow
	found, err := r.from(walletTable).
		Where(goqu.I("name").Eq(wallet)).
		Limit(1).
		ScanStructContext(ctx, &row)
	if err != nil {
		return nil, err
	}
	if !found {
		return nil, money.ErrNotFound{What: "wallet " + wallet}
	}

	return &row, nil
}

func (r *repo) GetWallet(ctx context.Context, wallet string) (*money.Wallet, error) {
	row, err := r.getWallet(ctx, wallet)
	if err != nil{
		return nil, err
	}

	return row.ToWallet(), nil
}

func (r *repo) AddWallet(ctx context.Context, username, wallet string, at time.Time) (*money.Wallet, error) {
	var row = &walletRow{
		Name : wallet,
		Username: username,
		CreatedAt: at,
	}
	insert := r.insert(walletTable).Rows(row).Returning(goqu.I("id")).Executor()
	var id int64
	_, err := insert.ScanValContext(ctx, &id)
	if err != nil {
		return nil, err
	}
	row.ID = id

	return row.ToWallet(), nil
}
