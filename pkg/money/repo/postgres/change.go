package postgres

import (
	"context"
	"fmt"
	"time"

	"github.com/doug-martin/goqu/v9"

	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/money"
)

const changeTable = "changes"

type changeRow struct {
	ID        int64     `db:"id" goqu:"skipinsert,skipupdate"`
	TransID   int64     `db:"trans_id" goqu:"skipupdate"`
	WalletID  int64     `db:"wallet_id" goqu:"skipupdate"`
	Amount    float64   `db:"amount" goqu:"skipupdate"`
	CreatedAt time.Time `db:"created_at" goqu:"skipupdate"`
}

func (row *changeRow) ToChange(wallet string, trans *money.Transaction) *money.Change {
	return &money.Change{
		Transaction: *trans,
		Wallet:      wallet,
		Amount:      row.Amount,
		CreatedAt:   row.CreatedAt,
	}
}

func (r *repo) AddChange(ctx context.Context, trans *money.Transaction, wallet string, amount float64, at time.Time) (*money.Change, error) {
	wRow, err := r.getWallet(ctx, wallet)
	if err != nil {
		return nil, err
	}

	var row = &changeRow{
		TransID:   trans.ID,
		WalletID:  wRow.ID,
		Amount:    amount,
		CreatedAt: at,
	}
	insert := r.insert(changeTable).Rows(row).Returning(goqu.I("id")).Executor()
	var id int64
	_, err = insert.ScanValContext(ctx, &id)
	if err != nil {
		return nil, err
	}
	row.ID = id

	return row.ToChange(wallet, trans), nil
}

func (r *repo) SumChanges(ctx context.Context, filters *money.ChangeFilters) (float64, error) {
	ds := r.from(changeTable).
		Select(goqu.COALESCE(
			goqu.SUM(goqu.I(changeTable+".amount")),
			goqu.L("0.0"),
		)).
		Join(goqu.T(walletTable), goqu.On(
			goqu.I(changeTable+".wallet_id").Eq(goqu.I(walletTable+".id")),
		)).
		Join(goqu.T(transactionTable), goqu.On(
			goqu.I(changeTable+".trans_id").Eq(goqu.I(transactionTable+".id")),
		))

	ds, err := r.applyChangeFilters(ds, filters)
	if err != nil {
		if err == ErrEmptyResultSet {
			// no point to hit the db
			return 0, nil
		}
		return 0, err
	}

	var sum float64
	_, err = ds.Executor().ScanValContext(ctx, &sum)
	return sum, err
}

func (r *repo) ListChanges(ctx context.Context, filters *money.ChangeFilters) ([]*money.Change, error) {
	ds := r.from(changeTable).
		Select(
			goqu.I(changeTable+".id").As(goqu.C("c.id")),
			goqu.I(changeTable+".trans_id").As(goqu.C("c.trans_id")),
			goqu.I(changeTable+".wallet_id").As(goqu.C("c.wallet_id")),
			goqu.I(changeTable+".amount").As(goqu.C("c.amount")),
			goqu.I(changeTable+".created_at").As(goqu.C("c.created_at")),

			goqu.I(walletTable+".id").As(goqu.C("w.id")),
			goqu.I(walletTable+".name").As(goqu.C("w.name")),
			goqu.I(walletTable+".username").As(goqu.C("w.username")),
			goqu.I(walletTable+".created_at").As(goqu.C("w.created_at")),

			goqu.I(transactionTable+".id").As(goqu.C("t.id")),
			goqu.I(transactionTable+".op").As(goqu.C("t.op")),
			goqu.I(transactionTable+".info").As(goqu.C("t.info")),
			goqu.I(transactionTable+".token").As(goqu.C("t.token")),
			goqu.I(transactionTable+".created_at").As(goqu.C("t.created_at")),
		).
		Join(goqu.T(walletTable), goqu.On(
			goqu.I(changeTable+".wallet_id").Eq(goqu.I(walletTable+".id")),
		)).
		Join(goqu.T(transactionTable), goqu.On(
			goqu.I(changeTable+".trans_id").Eq(goqu.I(transactionTable+".id")),
		))

	ds, err := r.applyChangeFilters(ds, filters)
	if err != nil {
		if err == ErrEmptyResultSet {
			// no point to hit the db
			return []*money.Change{}, nil
		}
		return nil, err
	}

	// scan would split every result row into three
	var rows []struct {
		ChangeRow      changeRow      `db:"c"`
		WalletRow      walletRow      `db:"w"`
		TransactionRow transactionRow `db:"t"`
	}

	err = ds.Executor().ScanStructsContext(ctx, &rows)
	if err != nil {
		return nil, err
	}

	var changes = make([]*money.Change, len(rows))

	for i, row := range rows {
		changes[i] = row.ChangeRow.ToChange(
			row.WalletRow.Name,
			row.TransactionRow.ToTransaction(),
		)
	}

	return changes, nil
}

var (
	ErrEmptyResultSet = fmt.Errorf("filters imply an empty result set")
	ErrBadOperation   = fmt.Errorf("filters contain bad opeation")
)

// applyChangeFilters adds constraints to a Dataset according to the given filters
//
func (r *repo) applyChangeFilters(ds *goqu.SelectDataset, filters *money.ChangeFilters) (*goqu.SelectDataset, error) {
	if filters == nil {
		return ds, nil
	}

	if names := filters.WalletEQ; names != nil {
		if len(names) == 0 {
			return nil, ErrEmptyResultSet
		}
		ds = ds.Where(goqu.I(walletTable + ".name").Eq(names))
	}
	if names := filters.WalletNE; names != nil {
		if len(names) > 0 {
			ds = ds.Where(goqu.I(walletTable + ".name").Neq(names))
		}
	}

	if pos := filters.AmountPositive; pos != nil {
		switch *pos {
		case true:
			ds = ds.Where(goqu.I(changeTable + ".amount").Gte(0))
		case false:
			ds = ds.Where(goqu.I(changeTable + ".amount").Lt(0))
		}
	}

	if op := filters.Operation; op != nil {
		switch *op {
		case money.OpDeposit, money.OpWithdraw, money.OpTransfer:
			ds = ds.Where(goqu.I(transactionTable + ".op").Eq(string(*op)))
		default:
			return nil, ErrBadOperation
		}
	}

	if t := filters.CreatedAtGE; !t.IsZero() {
		ds = ds.Where(goqu.I(changeTable + ".created_at").Gte(t))
	}

	if t := filters.CreatedAtLE; !t.IsZero() {
		ds = ds.Where(goqu.I(changeTable + ".created_at").Lte(t))
	}

	return ds, nil
}
