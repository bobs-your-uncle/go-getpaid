package money

import "time"

type Operation string

const (
	OpTransfer Operation = "transfer"
	OpDeposit  Operation = "deposit"
	OpWithdraw Operation = "withdraw"
)

// Wallet info
type Wallet struct {
	Name      string
	Username  string
	CreatedAt time.Time
}

// Wallet info + balance
type WalletWithBalance struct {
	Wallet
	Balance float64
}

// Transaction info
type Transaction struct {
	ID        int64
	Operation Operation
	Info      string
	Token     string
	CreatedAt time.Time
}

// Change - an immutable change to a wallet referencing specific transaction
type Change struct {
	Transaction

	Wallet    string
	Amount    float64
	CreatedAt time.Time
}

// ChangeFilters - a set of optional filters for the Change queries
type ChangeFilters struct {
	WalletEQ       []string
	WalletNE       []string
	AmountPositive *bool
	Operation      *Operation
	CreatedAtGE    time.Time
	CreatedAtLE    time.Time
}
