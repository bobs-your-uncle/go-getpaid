package money

import (
	"context"
	"errors"
	"fmt"
	"time"
)

const maxAttempts = 10 // max attempts to repeat a transaction

// service - implementation of the Service interface
type service struct {
	repo Repository
}

func NewService(repo Repository) Service {
	return &service{
		repo: repo,
	}
}

func (svc *service) ListWallets(ctx context.Context, username string) ([]*WalletWithBalance, error) {
	var withBalances []*WalletWithBalance

	// run in a transaction allowing `maxAttempts`
	err := svc.repo.Isolated(ctx, maxAttempts, func(txRepo Repository) error {
		wallets, err := txRepo.ListWallets(ctx, username)
		if err != nil {
			return fmt.Errorf("repo failed to list wallets: %w", err)
		}

		withBalances = make([]*WalletWithBalance, len(wallets))
		for i, wallet := range wallets {
			balance, err := txRepo.SumChanges(ctx, &ChangeFilters{
				WalletEQ: []string{wallet.Name},
			})
			if err != nil {
				return fmt.Errorf("repo failed to sum up wallet changes: %w", err)
			}
			withBalances[i] = &WalletWithBalance{
				Wallet:  *wallet,
				Balance: balance,
			}
		}

		return nil
	})
	if err != nil {
		return nil, err
	}

	return withBalances, nil
}

func (svc *service) GetWallet(ctx context.Context, username, walletName string) (*WalletWithBalance, error) {
	var withBalance *WalletWithBalance

	// run in a transaction allowing `maxAttempts`
	err := svc.repo.Isolated(ctx, maxAttempts, func(txRepo Repository) error {
		wallet, err := txRepo.GetWallet(ctx, walletName)
		if err != nil {
			return fmt.Errorf("repo failed to get a wallet: %w", err)
		}
		if wallet.Username != username {
			return ErrNotFound{What: "wallet "+walletName}
		}

		balance, err := txRepo.SumChanges(ctx, &ChangeFilters{
			WalletEQ: []string{wallet.Name},
		})
		if err != nil {
			return fmt.Errorf("repo failed to sum up wallet changes: %w", err)
		}
		withBalance = &WalletWithBalance{
			Wallet:  *wallet,
			Balance: balance,
		}

		return nil
	})

	return withBalance, err
}

func (svc *service) AddWallet(ctx context.Context, username, walletName string) (*WalletWithBalance, error) {
	var withBalance *WalletWithBalance

	// run in a transaction allowing `maxAttempts`
	err := svc.repo.Isolated(ctx, maxAttempts, func(txRepo Repository) error {
		// check the wallet does not exist
		_, err := txRepo.GetWallet(ctx, walletName)
		if err == nil {
			return ErrAlreadyExists{Name: walletName}
		}
		if !errors.As(err, &ErrNotFound{}) {
			return fmt.Errorf("repo failed to get a wallet: %w", err)
		}

		// add new wallet
		wallet, err := txRepo.AddWallet(ctx, username, walletName, time.Now())
		if err != nil {
			return fmt.Errorf("repo failed to add a wallet: %w", err)
		}
		withBalance = &WalletWithBalance{
			Wallet:  *wallet,
			Balance: 0,
		}

		return nil
	})

	return withBalance, err
}

func (svc *service) Transfer(ctx context.Context, username, token, fromName, toName string, amount float64) (*Change, *Change, error) {

	// sanity check
	if amount < 0 {
		return nil, nil, fmt.Errorf("attempt to transfer a negative amount")
	}

	var fromChange, toChange *Change

	// run in a transaction allowing `maxAttempts`
	err := svc.repo.Isolated(ctx, maxAttempts, func(txRepo Repository) error {
		// check the `from` wallet exists and it's ours
		fromWallet, err := txRepo.GetWallet(ctx, fromName)
		if err != nil {
			if errors.As(err, &ErrNotFound{}) {
				return err
			}
			return fmt.Errorf("repo failed to get a wallet: %w", err)
		}
		if fromWallet.Username != username {
			return ErrNotFound{What: "wallet "+fromName}
		}

		// check the `to` wallet exists
		_, err = txRepo.GetWallet(ctx, toName)
		if err != nil {
			if errors.As(err, &ErrNotFound{}) {
				return err
			}
			return fmt.Errorf("repo failed to get a wallet: %w", err)
		}

		// make sure the `from` wallet has enough funds
		balance, err := txRepo.SumChanges(ctx, &ChangeFilters{
			WalletEQ: []string{fromWallet.Name},
		})
		if err != nil {
			return fmt.Errorf("repo failed to sum up wallet changes: %w", err)
		}

		if balance < amount {
			return ErrNotEnoughFunds{Name: fromName}
		}

		// create new transaction and add changes
		now := time.Now()
		transaction, err := txRepo.AddTransaction(
			ctx,
			OpTransfer,
			fmt.Sprintf("transfer of funds from %v to %v", fromName, toName),
			token,
			now,
		)
		if err != nil {
			return fmt.Errorf("repo failed to add a transaction: %w", err)
		}

		fromChange, err = txRepo.AddChange(ctx, transaction, fromName, -amount, now)
		if err != nil {
			return fmt.Errorf("repo failed to add a change: %w", err)
		}

		toChange, err = txRepo.AddChange(ctx, transaction, toName, +amount, now)
		if err != nil {
			return fmt.Errorf("repo failed to add a change: %w", err)
		}

		return nil
	})
	if err != nil {
		return nil, nil, err
	}

	return fromChange, toChange, nil
}

func (svc *service) Deposit(ctx context.Context, username, token, walletName string, amount float64) (*Change, error) {

	// sanity check
	if amount < 0 {
		return nil, fmt.Errorf("attempt to deposit a negative amount")
	}

	var change *Change

	// run in a transaction allowing `maxAttempts`
	err := svc.repo.Isolated(ctx, maxAttempts, func(txRepo Repository) error {

		// check the wallet exists and it's ours
		wallet, err := txRepo.GetWallet(ctx, walletName)
		if err != nil {
			if errors.As(err, &ErrNotFound{}) {
				return err
			}
			return fmt.Errorf("repo failed to get a wallet: %w", err)
		}
		if wallet.Username != username {
			return ErrNotFound{What: "wallet "+walletName}
		}

		// create new transaction and add a change
		now := time.Now()
		transaction, err := txRepo.AddTransaction(
			ctx,
			OpDeposit,
			fmt.Sprintf("deposit of funds to %v", walletName),
			token,
			now,
		)
		if err != nil {
			return fmt.Errorf("repo failed to add a transaction: %w", err)
		}

		change, err = txRepo.AddChange(ctx, transaction, walletName, +amount, now)
		if err != nil {
			return fmt.Errorf("repo failed to add a wallet: %w", err)
		}

		return nil
	})

	return change, err
}

func (svc *service) Withdraw(ctx context.Context, username, token, walletName string, amount float64) (*Change, error) {

	// sanity check
	if amount < 0 {
		return nil, fmt.Errorf("attempt to withdraw a negative amount")
	}

	var change *Change

	// run in a transaction allowing `maxAttempts`
	err := svc.repo.Isolated(ctx, maxAttempts, func(txRepo Repository) error {

		// check the wallet exists and it's ours
		wallet, err := txRepo.GetWallet(ctx, walletName)
		if err != nil {
			if errors.As(err, &ErrNotFound{}) {
				return err
			}
			return fmt.Errorf("repo failed to get a wallet: %w", err)
		}
		if wallet.Username != username {
			return ErrNotFound{What: "wallet "+walletName}
		}

		// make sure the wallet has enough funds
		balance, err := txRepo.SumChanges(ctx, &ChangeFilters{
			WalletEQ: []string{walletName},
		})
		if err != nil {
			return fmt.Errorf("repo failed to sum up wallet changes: %w", err)
		}

		if balance < amount {
			return ErrNotEnoughFunds{Name: walletName}
		}

		// create new transaction and add a change
		now := time.Now()
		transaction, err := txRepo.AddTransaction(
			ctx,
			OpWithdraw,
			fmt.Sprintf("withdraw of funds from %v", walletName),
			token,
			now,
		)
		if err != nil {
			return fmt.Errorf("repo failed to add a transaction: %w", err)
		}

		change, err = txRepo.AddChange(ctx, transaction, walletName, -amount, now)
		if err != nil {
			return fmt.Errorf("repo failed to add a wallet: %w", err)
		}

		return nil
	})

	return change, err
}

func (svc *service) GetHistory(ctx context.Context, username, walletName string, filters *ChangeFilters) ([]*Change, error) {
	var changes []*Change

	// run in a transaction allowing `maxAttempts`
	err := svc.repo.Isolated(ctx, maxAttempts, func(txRepo Repository) error {

		if filters == nil {
			filters = new(ChangeFilters)
		}

		filters.WalletEQ = []string{walletName} // overwrite whatever came in

		// check the wallet exists and it's ours
		wallet, err := txRepo.GetWallet(ctx, walletName)
		if err != nil {
			if errors.As(err, &ErrNotFound{}) {
				return err
			}
			return fmt.Errorf("repo failed to get a wallet: %w", err)
		}
		if wallet.Username != username {
			return ErrNotFound{What: "wallet "+walletName}
		}

		changes, err = txRepo.ListChanges(ctx, filters)
		if err != nil {
			return fmt.Errorf("repo failed to list changes: %w", err)
		}

		return nil
	})
	if err != nil {
		return nil, err
	}

	return changes, nil
}
