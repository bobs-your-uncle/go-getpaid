package money

import "fmt"

// ErrAlreadyExists - an error indicating a wallet already exists
type ErrAlreadyExists struct {
	Name string
}

func (err ErrAlreadyExists) Error() string {
	return "wallet already exists: " + err.Name
}

// ErrNotEnoughFunds - an error indicating a wallet already exists
type ErrNotEnoughFunds struct {
	Name string
}

func (err ErrNotEnoughFunds) Error() string {
	return fmt.Sprintf("not enough funds in wallet %v", err.Name)
}

// ErrNotFound - an error indicating something's not found
type ErrNotFound struct {
	What string
}

func (err ErrNotFound) Error() string {
	return fmt.Sprintf("%v not found", err.What)
}
