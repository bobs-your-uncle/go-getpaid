package money

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
)

var anyCtx = mock.MatchedBy(func(_ context.Context) bool { return true })

func TestNewServiceGetsCreated(t *testing.T) {
	var r = new(MockRepository)

	// --- business logic ---
	s := NewService(r)
	// ----------------------

	assert.NotNil(t, s)

	r.AssertExpectations(t)
}

// -- Service.GetWallet --

type TestSuiteListWallets struct {
	suite.Suite
}

func TestListWallets(t *testing.T) {
	suite.Run(t, new(TestSuiteListWallets))
}

func (suite *TestSuiteListWallets) TestRepoFailsToListWallets() {
	const (
		username = "test-user"
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		e = errors.New("TEST ERROR")
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("ListWallets", anyCtx, username).Once().Return(nil, e)

	// --- business logic ---
	wallets, _ := s.ListWallets(ctx, username)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, wallets)

	r.AssertExpectations(t)
}

func (suite *TestSuiteListWallets) TestRepoFailsToSumChanges() {
	const (
		username = "test-user"
	)
	var (
		t  = suite.T()
		r  = new(MockRepository)
		s  = NewService(r)
		w  = &Wallet{Name: "wallet", Username: username}
		f  = &ChangeFilters{WalletEQ: []string{w.Name}}
		e  = errors.New("TEST ERROR")
		we = fmt.Errorf("repo failed to sum up wallet changes: %w", e)
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(we)
	r.On("ListWallets", anyCtx, username).Once().Return([]*Wallet{w}, nil)
	r.On("SumChanges", anyCtx, f).Once().Return(0.0, e)

	// --- business logic ---
	wallets, _ := s.ListWallets(ctx, username)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, wallets)

	r.AssertExpectations(t)
}

func (suite *TestSuiteListWallets) TestRepoReturnsNoWallets() {
	const (
		username = "test-user"
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("ListWallets", anyCtx, username).Once().Return([]*Wallet{}, nil)

	// --- business logic ---
	wallets, _ := s.ListWallets(ctx, username)
	// ----------------------

	assert.NoError(t, resErr)
	assert.NotNil(t, wallets)
	assert.Len(t, wallets, 0)

	r.AssertExpectations(t)
}

func (suite *TestSuiteListWallets) TestRepoReturnsTwoWallets() {
	const (
		username = "test-user"
		balance1 = 123.0
		balance2 = 456.0
	)
	var (
		t   = suite.T()
		r   = new(MockRepository)
		s   = NewService(r)
		w1  = &Wallet{Name: "wallet1", Username: username}
		w2  = &Wallet{Name: "wallet2", Username: username}
		wb1 = &WalletWithBalance{Wallet: *w1, Balance: balance1}
		wb2 = &WalletWithBalance{Wallet: *w2, Balance: balance2}
		f1  = &ChangeFilters{WalletEQ: []string{w1.Name}}
		f2  = &ChangeFilters{WalletEQ: []string{w2.Name}}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("ListWallets", anyCtx, username).Once().Return([]*Wallet{w1, w2}, nil)
	r.On("SumChanges", anyCtx, f1).Once().Return(balance1, nil)
	r.On("SumChanges", anyCtx, f2).Once().Return(balance2, nil)

	// --- business logic ---
	wallets, _ := s.ListWallets(ctx, username)
	// ----------------------

	assert.NoError(t, resErr)
	assert.Equal(t, []*WalletWithBalance{wb1, wb2}, wallets)

	r.AssertExpectations(t)
}

// -- Service.GetWallet --

type TestSuiteGetWallet struct {
	suite.Suite
}

func TestGetWallet(t *testing.T) {
	suite.Run(t, new(TestSuiteGetWallet))
}

func (suite *TestSuiteGetWallet) TestRepoFailsToGetWallet() {
	const (
		username   = "test-user"
		walletName = "test-wallet"
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		e = errors.New("TEST ERROR")
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(nil, e)

	// --- business logic ---
	wallet, _ := s.GetWallet(ctx, username, walletName)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, wallet)

	r.AssertExpectations(t)
}

func (suite *TestSuiteGetWallet) TestDetectsWrongUser() {
	const (
		username   = "test-user"
		wrongUser  = "wrong-user"
		walletName = "test-wallet"
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		w = &Wallet{Name: walletName, Username: wrongUser}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(w, nil)

	// --- business logic ---
	wallet, _ := s.GetWallet(ctx, username, walletName)
	// ----------------------

	require.Error(t, resErr)
	assert.EqualError(t, resErr, ErrNotFound{What: "wallet " + walletName}.Error())
	assert.Nil(t, wallet)

	r.AssertExpectations(t)
}

func (suite *TestSuiteGetWallet) TestRepoFailsToSumChanges() {
	const (
		username   = "test-user"
		walletName = "test-wallet"
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		w = &Wallet{Name: walletName, Username: username}
		f = &ChangeFilters{WalletEQ: []string{w.Name}}
		e = errors.New("TEST ERROR")
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(w, nil)
	r.On("SumChanges", anyCtx, f).Once().Return(0.0, e)

	// --- business logic ---
	wallet, _ := s.GetWallet(ctx, username, walletName)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, wallet)

	r.AssertExpectations(t)
}

func (suite *TestSuiteGetWallet) TestRepoGetsWallet() {
	const (
		username   = "test-user"
		walletName = "test-wallet"
		balance    = 555.0
	)
	var (
		t  = suite.T()
		r  = new(MockRepository)
		s  = NewService(r)
		w  = &Wallet{Name: walletName, Username: username}
		f  = &ChangeFilters{WalletEQ: []string{w.Name}}
		wb = &WalletWithBalance{Wallet: *w, Balance: balance}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(w, nil)
	r.On("SumChanges", anyCtx, f).Once().Return(balance, nil)

	// --- business logic ---
	wallet, _ := s.GetWallet(ctx, username, walletName)
	// ----------------------

	assert.NoError(t, resErr)
	assert.Equal(t, wb, wallet)

	r.AssertExpectations(t)
}

// -- Service.AddWallet --

type TestSuiteAddWallet struct {
	suite.Suite
}

func TestAddWallet(t *testing.T) {
	suite.Run(t, new(TestSuiteAddWallet))
}

func (suite *TestSuiteAddWallet) TestRepoFailsToGetWallet() {
	const (
		username   = "test-user"
		walletName = "test-wallet"
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		e = errors.New("TEST ERROR")
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(nil, e)

	// --- business logic ---
	wallet, _ := s.AddWallet(ctx, username, walletName)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, wallet)

	r.AssertExpectations(t)
}

func (suite *TestSuiteAddWallet) TestAlreadyExists() {
	const (
		username   = "test-user"
		walletName = "test-wallet"
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		w = &Wallet{Name: walletName, Username: username}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(w, nil)

	// --- business logic ---
	wallet, _ := s.AddWallet(ctx, username, walletName)
	// ----------------------

	require.Error(t, resErr)
	assert.EqualError(t, resErr, ErrAlreadyExists{Name: walletName}.Error())
	assert.Nil(t, wallet)

	r.AssertExpectations(t)
}

func (suite *TestSuiteAddWallet) TestRepoFailsToAddWallet() {
	const (
		username   = "test-user"
		walletName = "test-wallet"
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		e = errors.New("TEST ERROR")
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
		before = time.Now()
		timeFn = mock.MatchedBy(func(t time.Time) bool {
			return t.Sub(before) >= 0
		})
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(nil, ErrNotFound{What: "wallet " + walletName})
	r.On("AddWallet", anyCtx, username, walletName, timeFn).Once().Return(nil, e)

	// --- business logic ---
	wallet, _ := s.AddWallet(ctx, username, walletName)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, wallet)

	r.AssertExpectations(t)
}

func (suite *TestSuiteAddWallet) TestRepoAddsWallet() {
	const (
		username   = "test-user"
		walletName = "test-wallet"
	)
	var (
		t  = suite.T()
		r  = new(MockRepository)
		s  = NewService(r)
		w  = &Wallet{Name: walletName, Username: username}
		wb = &WalletWithBalance{Wallet: *w, Balance: 0}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
		before = time.Now()
		timeFn = mock.MatchedBy(func(t time.Time) bool {
			return t.Sub(before) >= 0
		})
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(nil, ErrNotFound{What: "wallet " + walletName})
	r.On("AddWallet", anyCtx, username, walletName, timeFn).Once().Return(w, nil)

	// --- business logic ---
	wallet, _ := s.AddWallet(ctx, username, walletName)
	// ----------------------

	assert.NoError(t, resErr)
	assert.Equal(t, wb, wallet)

	r.AssertExpectations(t)
}

// -- Service.Transfer --

type TestSuiteTransfer struct {
	suite.Suite
}

func TestTransfer(t *testing.T) {
	suite.Run(t, new(TestSuiteTransfer))
}

func (suite *TestSuiteTransfer) TestDetectsNegativeAmount() {
	const (
		username = "test-user"
		token    = "test-token"
		fromName = "test-wallet-from"
		toName   = "test-wallet-to"
		amount   = -123.0
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// --- business logic ---
	changeFrom, changeTo, err := s.Transfer(ctx, username, token, fromName, toName, amount)
	// ----------------------

	require.Error(t, err)
	assert.Contains(t, err.Error(), "negative amount")
	assert.Nil(t, changeFrom)
	assert.Nil(t, changeTo)

	r.AssertExpectations(t)
}

func (suite *TestSuiteTransfer) TestRepoFailsToGetFromWallet() {
	const (
		username = "test-user"
		token    = "test-token"
		fromName = "test-wallet-from"
		toName   = "test-wallet-to"
		amount   = 123.45
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		e = errors.New("TEST ERROR")
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, fromName).Once().Return(nil, e)

	// --- business logic ---
	changeFrom, changeTo, _ := s.Transfer(ctx, username, token, fromName, toName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, changeFrom)
	assert.Nil(t, changeTo)

	r.AssertExpectations(t)
}

func (suite *TestSuiteTransfer) TestWrongUser() {
	const (
		username  = "test-user"
		wrongUser = "wrong-user"
		token     = "test-token"
		fromName  = "test-wallet-from"
		toName    = "test-wallet-to"
		amount    = 123.45
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		w = &Wallet{Name: fromName, Username: wrongUser}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, fromName).Once().Return(w, nil)

	// --- business logic ---
	changeFrom, changeTo, _ := s.Transfer(ctx, username, token, fromName, toName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.EqualError(t, resErr, ErrNotFound{What: "wallet " + fromName}.Error())
	assert.Nil(t, changeFrom)
	assert.Nil(t, changeTo)

	r.AssertExpectations(t)
}

func (suite *TestSuiteTransfer) TestRepoFailsToGetToWallet() {
	const (
		username = "test-user"
		token    = "test-token"
		fromName = "test-wallet-from"
		toName   = "test-wallet-to"
		amount   = 123.45
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		w = &Wallet{Name: fromName, Username: username}
		e = errors.New("TEST ERROR")
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, fromName).Once().Return(w, nil)
	r.On("GetWallet", anyCtx, toName).Once().Return(nil, e)

	// --- business logic ---
	changeFrom, changeTo, _ := s.Transfer(ctx, username, token, fromName, toName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, changeFrom)
	assert.Nil(t, changeTo)

	r.AssertExpectations(t)
}

func (suite *TestSuiteTransfer) TestRepoFailsToSumChanges() {
	const (
		username  = "test-user"
		otherUser = "other-user"
		token     = "test-token"
		fromName  = "test-wallet-from"
		toName    = "test-wallet-to"
		amount    = 123.45
	)
	var (
		t  = suite.T()
		r  = new(MockRepository)
		s  = NewService(r)
		wf = &Wallet{Name: fromName, Username: username}
		wt = &Wallet{Name: toName, Username: otherUser}
		e  = errors.New("TEST ERROR")
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, fromName).Once().Return(wf, nil)
	r.On("GetWallet", anyCtx, toName).Once().Return(wt, nil)
	r.On("SumChanges", anyCtx, &ChangeFilters{WalletEQ: []string{fromName}}).Once().Return(0.0, e)

	// --- business logic ---
	changeFrom, changeTo, _ := s.Transfer(ctx, username, token, fromName, toName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, changeFrom)
	assert.Nil(t, changeTo)

	r.AssertExpectations(t)
}

func (suite *TestSuiteTransfer) TestNotEnoughFunds() {
	const (
		username  = "test-user"
		otherUser = "other-user"
		token     = "test-token"
		fromName  = "test-wallet-from"
		toName    = "test-wallet-to"
		balance   = 110.00
		amount    = 123.45
	)
	var (
		t  = suite.T()
		r  = new(MockRepository)
		s  = NewService(r)
		wf = &Wallet{Name: fromName, Username: username}
		wt = &Wallet{Name: toName, Username: otherUser}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, fromName).Once().Return(wf, nil)
	r.On("GetWallet", anyCtx, toName).Once().Return(wt, nil)
	r.On("SumChanges", anyCtx, &ChangeFilters{WalletEQ: []string{fromName}}).Once().Return(balance, nil)

	// --- business logic ---
	changeFrom, changeTo, _ := s.Transfer(ctx, username, token, fromName, toName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.EqualError(t, resErr, ErrNotEnoughFunds{Name: fromName}.Error())
	assert.Nil(t, changeFrom)
	assert.Nil(t, changeTo)

	r.AssertExpectations(t)
}

func (suite *TestSuiteTransfer) TestRepoFailsToAddTransaction() {
	const (
		username  = "test-user"
		otherUser = "other-user"
		token     = "test-token"
		fromName  = "test-wallet-from"
		toName    = "test-wallet-to"
		balance   = 130.00
		amount    = 123.45
	)
	var (
		t  = suite.T()
		r  = new(MockRepository)
		s  = NewService(r)
		wf = &Wallet{Name: fromName, Username: username}
		wt = &Wallet{Name: toName, Username: otherUser}
		e  = errors.New("NEW ERROR")
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
		before = time.Now()
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, fromName).Once().Return(wf, nil)
	r.On("GetWallet", anyCtx, toName).Once().Return(wt, nil)
	r.On("SumChanges", anyCtx, &ChangeFilters{WalletEQ: []string{fromName}}).Once().Return(balance, nil)
	r.On("AddTransaction",
		anyCtx,
		OpTransfer,
		mock.MatchedBy(func(msg string) bool { return strings.Contains(msg, "transfer of funds from "+fromName) }),
		token,
		mock.MatchedBy(func(t time.Time) bool { return t.Sub(before) >= 0 }),
	).Once().Return(nil, e)

	// --- business logic ---
	changeFrom, changeTo, _ := s.Transfer(ctx, username, token, fromName, toName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, changeFrom)
	assert.Nil(t, changeTo)

	r.AssertExpectations(t)
}

func (suite *TestSuiteTransfer) TestRepoFailsToAddFromChange() {
	const (
		username  = "test-user"
		otherUser = "other-user"
		token     = "test-token"
		fromName  = "test-wallet-from"
		toName    = "test-wallet-to"
		balance   = 130.00
		amount    = 123.45
	)
	var before = time.Now()
	var (
		t  = suite.T()
		r  = new(MockRepository)
		s  = NewService(r)
		wf = &Wallet{Name: fromName, Username: username}
		wt = &Wallet{Name: toName, Username: otherUser}
		e  = errors.New("NEW ERROR")
		tx = &Transaction{
			ID:        1789,
			Operation: OpTransfer,
			Info:      "transfer funds from " + fromName + " to " + toName,
			Token:     token,
			CreatedAt: before,
		}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
		timeFn = mock.MatchedBy(func(t time.Time) bool { return t.Sub(before) >= 0 })
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, fromName).Once().Return(wf, nil)
	r.On("GetWallet", anyCtx, toName).Once().Return(wt, nil)
	r.On("SumChanges", anyCtx, &ChangeFilters{WalletEQ: []string{fromName}}).Once().Return(balance, nil)
	r.On("AddTransaction",
		anyCtx,
		OpTransfer,
		mock.MatchedBy(func(msg string) bool { return strings.Contains(msg, "transfer of funds from "+fromName) }),
		token,
		mock.MatchedBy(func(t time.Time) bool { return t.Sub(before) >= 0 }),
	).Once().Return(tx, nil)
	r.On("AddChange", anyCtx, tx, fromName, -amount, timeFn).Once().Return(nil, e)

	// --- business logic ---
	changeFrom, changeTo, _ := s.Transfer(ctx, username, token, fromName, toName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, changeFrom)
	assert.Nil(t, changeTo)

	r.AssertExpectations(t)
}

func (suite *TestSuiteTransfer) TestRepoFailsToAddToChange() {
	const (
		username  = "test-user"
		otherUser = "other-user"
		token     = "test-token"
		fromName  = "test-wallet-from"
		toName    = "test-wallet-to"
		balance   = 130.00
		amount    = 123.45
	)
	var before = time.Now()
	var (
		t  = suite.T()
		r  = new(MockRepository)
		s  = NewService(r)
		wf = &Wallet{Name: fromName, Username: username}
		wt = &Wallet{Name: toName, Username: otherUser}
		e  = errors.New("NEW ERROR")
		tx = &Transaction{
			ID:        1789,
			Operation: OpTransfer,
			Info:      "transfer funds from " + fromName + " to " + toName,
			Token:     token,
			CreatedAt: before,
		}
		cf = &Change{
			Transaction: *tx,
			Wallet:      fromName,
			Amount:      -amount,
			CreatedAt:   before,
		}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
		timeFn = mock.MatchedBy(func(t time.Time) bool { return t.Sub(before) >= 0 })
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(fmt.Errorf("repo failed to add a change: %w", e))
	r.On("GetWallet", anyCtx, fromName).Once().Return(wf, nil)
	r.On("GetWallet", anyCtx, toName).Once().Return(wt, nil)
	r.On("SumChanges", anyCtx, &ChangeFilters{WalletEQ: []string{fromName}}).Once().Return(balance, nil)
	r.On("AddTransaction",
		anyCtx,
		OpTransfer,
		mock.MatchedBy(func(msg string) bool { return strings.Contains(msg, "transfer of funds from "+fromName) }),
		token,
		mock.MatchedBy(func(t time.Time) bool { return t.Sub(before) >= 0 }),
	).Once().Return(tx, nil)
	r.On("AddChange", anyCtx, tx, fromName, -amount, timeFn).Once().Return(cf, nil)
	r.On("AddChange", anyCtx, tx, toName, +amount, timeFn).Once().Return(nil, e)

	// --- business logic ---
	changeFrom, changeTo, _ := s.Transfer(ctx, username, token, fromName, toName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, changeFrom)
	assert.Nil(t, changeTo)

	r.AssertExpectations(t)
}

func (suite *TestSuiteTransfer) TestOK() {
	const (
		username  = "test-user"
		otherUser = "other-user"
		token     = "test-token"
		fromName  = "test-wallet-from"
		toName    = "test-wallet-to"
		balance   = 130.00
		amount    = 123.45
	)
	var before = time.Now()
	var (
		t  = suite.T()
		r  = new(MockRepository)
		s  = NewService(r)
		wf = &Wallet{Name: fromName, Username: username}
		wt = &Wallet{Name: toName, Username: otherUser}
		tx = &Transaction{
			ID:        1789,
			Operation: OpTransfer,
			Info:      "transfer funds from " + fromName + " to " + toName,
			Token:     token,
			CreatedAt: before,
		}
		cf = &Change{
			Transaction: *tx,
			Wallet:      fromName,
			Amount:      -amount,
			CreatedAt:   before,
		}
		ct = &Change{
			Transaction: *tx,
			Wallet:      toName,
			Amount:      +amount,
			CreatedAt:   before,
		}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
		timeFn = mock.MatchedBy(func(t time.Time) bool { return t.Sub(before) >= 0 })
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, fromName).Once().Return(wf, nil)
	r.On("GetWallet", anyCtx, toName).Once().Return(wt, nil)
	r.On("SumChanges", anyCtx, &ChangeFilters{WalletEQ: []string{fromName}}).Once().Return(balance, nil)
	r.On("AddTransaction",
		anyCtx,
		OpTransfer,
		mock.MatchedBy(func(msg string) bool { return strings.Contains(msg, "transfer of funds from "+fromName) }),
		token,
		mock.MatchedBy(func(t time.Time) bool { return t.Sub(before) >= 0 }),
	).Once().Return(tx, nil)
	r.On("AddChange", anyCtx, tx, fromName, -amount, timeFn).Once().Return(cf, nil)
	r.On("AddChange", anyCtx, tx, toName, +amount, timeFn).Once().Return(ct, nil)

	// --- business logic ---
	changeFrom, changeTo, _ := s.Transfer(ctx, username, token, fromName, toName, amount)
	// ----------------------

	assert.NoError(t, resErr)
	assert.Equal(t, cf, changeFrom)
	assert.Equal(t, ct, changeTo)

	r.AssertExpectations(t)
}

// -- Service.Deposit --

type TestSuiteDeposit struct {
	suite.Suite
}

func TestDeposit(t *testing.T) {
	suite.Run(t, new(TestSuiteDeposit))
}

func (suite *TestSuiteDeposit) TestDetectsNegativeAmount() {
	const (
		username   = "test-user"
		token      = "test-token"
		walletName = "test-wallet"
		amount     = -123.0
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// --- business logic ---
	change, err := s.Deposit(ctx, username, token, walletName, amount)
	// ----------------------

	require.Error(t, err)
	assert.Contains(t, err.Error(), "negative amount")
	assert.Nil(t, change)

	r.AssertExpectations(t)
}

func (suite *TestSuiteDeposit) TestRepoFailsToGetWallet() {
	const (
		username   = "test-user"
		token      = "test-token"
		walletName = "test-wallet"
		amount     = 123.45
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		e = errors.New("TEST ERROR")
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(nil, e)

	// --- business logic ---
	change, _ := s.Deposit(ctx, username, token, walletName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, change)

	r.AssertExpectations(t)
}

func (suite *TestSuiteDeposit) TestNotFound() {
	const (
		username   = "test-user"
		token      = "test-token"
		walletName = "test-wallet"
		amount     = 123.45
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		e = ErrNotFound{What: "wallet " + walletName}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(nil, e)

	// --- business logic ---
	change, _ := s.Deposit(ctx, username, token, walletName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.EqualError(t, resErr, e.Error())
	assert.Nil(t, change)

	r.AssertExpectations(t)
}

func (suite *TestSuiteDeposit) TestWrongUser() {
	const (
		username   = "test-user"
		wrongUser  = "wrong-user"
		token      = "test-token"
		walletName = "test-wallet-from"
		amount     = 123.45
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		w = &Wallet{Name: walletName, Username: wrongUser}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(w, nil)

	// --- business logic ---
	change, _ := s.Deposit(ctx, username, token, walletName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.EqualError(t, resErr, ErrNotFound{What: "wallet " + walletName}.Error())
	assert.Nil(t, change)

	r.AssertExpectations(t)
}

func (suite *TestSuiteDeposit) TestRepoFailsToAddTransaction() {
	const (
		username   = "test-user"
		token      = "test-token"
		walletName = "test-wallet-from"
		amount     = 123.45
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		w = &Wallet{Name: walletName, Username: username}
		e = errors.New("NEW ERROR")
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
		before = time.Now()
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(w, nil)
	r.On("AddTransaction",
		anyCtx,
		OpDeposit,
		mock.MatchedBy(func(msg string) bool { return strings.Contains(msg, "deposit of funds to "+walletName) }),
		token,
		mock.MatchedBy(func(t time.Time) bool { return t.Sub(before) >= 0 }),
	).Once().Return(nil, e)

	// --- business logic ---
	change, _ := s.Deposit(ctx, username, token, walletName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, change)

	r.AssertExpectations(t)
}

func (suite *TestSuiteDeposit) TestRepoFailsToAddChange() {
	const (
		username   = "test-user"
		token      = "test-token"
		walletName = "test-wallet-from"
		amount     = 123.45
	)
	var before = time.Now()
	var (
		t  = suite.T()
		r  = new(MockRepository)
		s  = NewService(r)
		w  = &Wallet{Name: walletName, Username: username}
		tx = &Transaction{
			ID:        1789,
			Operation: OpDeposit,
			Info:      "deposit funds to " + walletName,
			Token:     token,
			CreatedAt: before,
		}
		e = errors.New("NEW ERROR")
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
		timeFn = mock.MatchedBy(func(t time.Time) bool { return t.Sub(before) >= 0 })
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(w, nil)
	r.On("AddTransaction",
		anyCtx,
		OpDeposit,
		mock.MatchedBy(func(msg string) bool { return strings.Contains(msg, "deposit of funds to "+walletName) }),
		token,
		mock.MatchedBy(func(t time.Time) bool { return t.Sub(before) >= 0 }),
	).Once().Return(tx, nil)
	r.On("AddChange", anyCtx, tx, walletName, +amount, timeFn).Once().Return(nil, e)

	// --- business logic ---
	change, _ := s.Deposit(ctx, username, token, walletName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, change)

	r.AssertExpectations(t)
}

func (suite *TestSuiteDeposit) TestOK() {
	const (
		username   = "test-user"
		token      = "test-token"
		walletName = "test-wallet-from"
		amount     = 123.45
	)
	var before = time.Now()
	var (
		t  = suite.T()
		r  = new(MockRepository)
		s  = NewService(r)
		w  = &Wallet{Name: walletName, Username: username}
		tx = &Transaction{
			ID:        1789,
			Operation: OpDeposit,
			Info:      "deposit funds to " + walletName,
			Token:     token,
			CreatedAt: before,
		}
		ch = &Change{
			Transaction: *tx,
			Wallet:      walletName,
			Amount:      +amount,
			CreatedAt:   before,
		}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
		timeFn = mock.MatchedBy(func(t time.Time) bool { return t.Sub(before) >= 0 })
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(w, nil)
	r.On("AddTransaction",
		anyCtx,
		OpDeposit,
		mock.MatchedBy(func(msg string) bool { return strings.Contains(msg, "deposit of funds to "+walletName) }),
		token,
		mock.MatchedBy(func(t time.Time) bool { return t.Sub(before) >= 0 }),
	).Once().Return(tx, nil)
	r.On("AddChange", anyCtx, tx, walletName, +amount, timeFn).Once().Return(ch, nil)

	// --- business logic ---
	change, _ := s.Deposit(ctx, username, token, walletName, amount)
	// ----------------------

	assert.NoError(t, resErr)
	assert.Equal(t, ch, change)

	r.AssertExpectations(t)
}

// -- Service.Withdraw --

type TestSuiteWithdraw struct {
	suite.Suite
}

func TestWithdraw(t *testing.T) {
	suite.Run(t, new(TestSuiteWithdraw))
}

func (suite *TestSuiteWithdraw) TestDetectsNegativeAmount() {
	const (
		username   = "test-user"
		token      = "test-token"
		walletName = "test-wallet"
		amount     = -123.0
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// --- business logic ---
	change, err := s.Withdraw(ctx, username, token, walletName, amount)
	// ----------------------

	require.Error(t, err)
	assert.Contains(t, err.Error(), "negative amount")
	assert.Nil(t, change)

	r.AssertExpectations(t)
}

func (suite *TestSuiteWithdraw) TestRepoFailsToGetWallet() {
	const (
		username   = "test-user"
		token      = "test-token"
		walletName = "test-wallet"
		amount     = 123.45
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		e = errors.New("TEST ERROR")
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(nil, e)

	// --- business logic ---
	change, _ := s.Withdraw(ctx, username, token, walletName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, change)

	r.AssertExpectations(t)
}

func (suite *TestSuiteWithdraw) TestNotFound() {
	const (
		username   = "test-user"
		token      = "test-token"
		walletName = "test-wallet"
		amount     = 123.45
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		e = ErrNotFound{What: "wallet " + walletName}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(nil, e)

	// --- business logic ---
	change, _ := s.Withdraw(ctx, username, token, walletName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.EqualError(t, resErr, e.Error())
	assert.Nil(t, change)

	r.AssertExpectations(t)
}

func (suite *TestSuiteWithdraw) TestWrongUser() {
	const (
		username   = "test-user"
		wrongUser  = "wrong-user"
		token      = "test-token"
		walletName = "test-wallet-from"
		amount     = 123.45
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		w = &Wallet{Name: walletName, Username: wrongUser}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(w, nil)

	// --- business logic ---
	change, _ := s.Withdraw(ctx, username, token, walletName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.EqualError(t, resErr, ErrNotFound{What: "wallet " + walletName}.Error())
	assert.Nil(t, change)

	r.AssertExpectations(t)
}

func (suite *TestSuiteWithdraw) TestRepoFailsToSumChanges() {
	const (
		username   = "test-user"
		otherUser  = "other-user"
		token      = "test-token"
		walletName = "test-wallet"
		amount     = 123.45
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		w = &Wallet{Name: walletName, Username: username}
		e = errors.New("TEST ERROR")
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(w, nil)
	r.On("SumChanges", anyCtx, &ChangeFilters{WalletEQ: []string{walletName}}).Once().Return(0.0, e)

	// --- business logic ---
	change, _ := s.Withdraw(ctx, username, token, walletName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, change)

	r.AssertExpectations(t)
}

func (suite *TestSuiteWithdraw) TestNotEnoughFunds() {
	const (
		username   = "test-user"
		otherUser  = "other-user"
		token      = "test-token"
		walletName = "test-wallet"
		balance    = 121.99
		amount     = 123.45
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		w = &Wallet{Name: walletName, Username: username}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(w, nil)
	r.On("SumChanges", anyCtx, &ChangeFilters{WalletEQ: []string{walletName}}).Once().Return(balance, nil)

	// --- business logic ---
	change, _ := s.Withdraw(ctx, username, token, walletName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.EqualError(t, resErr, ErrNotEnoughFunds{Name: walletName}.Error())
	assert.Nil(t, change)

	r.AssertExpectations(t)
}

func (suite *TestSuiteWithdraw) TestRepoFailsToAddTransaction() {
	const (
		username   = "test-user"
		token      = "test-token"
		walletName = "test-wallet-from"
		balance    = 123.99
		amount     = 123.45
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		w = &Wallet{Name: walletName, Username: username}
		e = errors.New("NEW ERROR")
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
		before = time.Now()
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(w, nil)
	r.On("SumChanges", anyCtx, &ChangeFilters{WalletEQ: []string{walletName}}).Once().Return(balance, nil)
	r.On("AddTransaction",
		anyCtx,
		OpWithdraw,
		mock.MatchedBy(func(msg string) bool { return strings.Contains(msg, "withdraw of funds from "+walletName) }),
		token,
		mock.MatchedBy(func(t time.Time) bool { return t.Sub(before) >= 0 }),
	).Once().Return(nil, e)

	// --- business logic ---
	change, _ := s.Withdraw(ctx, username, token, walletName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, change)

	r.AssertExpectations(t)
}

func (suite *TestSuiteWithdraw) TestRepoFailsToAddChange() {
	const (
		username   = "test-user"
		token      = "test-token"
		walletName = "test-wallet-from"
		balance    = 555.55
		amount     = 123.45
	)
	var before = time.Now()
	var (
		t  = suite.T()
		r  = new(MockRepository)
		s  = NewService(r)
		w  = &Wallet{Name: walletName, Username: username}
		tx = &Transaction{
			ID:        1789,
			Operation: OpWithdraw,
			Info:      "withdraw funds from " + walletName,
			Token:     token,
			CreatedAt: before,
		}
		e = errors.New("NEW ERROR")
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
		timeFn = mock.MatchedBy(func(t time.Time) bool { return t.Sub(before) >= 0 })
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(w, nil)
	r.On("SumChanges", anyCtx, &ChangeFilters{WalletEQ: []string{walletName}}).Once().Return(balance, nil)
	r.On("AddTransaction",
		anyCtx,
		OpWithdraw,
		mock.MatchedBy(func(msg string) bool { return strings.Contains(msg, "withdraw of funds from "+walletName) }),
		token,
		mock.MatchedBy(func(t time.Time) bool { return t.Sub(before) >= 0 }),
	).Once().Return(tx, nil)
	r.On("AddChange", anyCtx, tx, walletName, -amount, timeFn).Once().Return(nil, e)

	// --- business logic ---
	change, _ := s.Withdraw(ctx, username, token, walletName, amount)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, change)

	r.AssertExpectations(t)
}

func (suite *TestSuiteWithdraw) TestOK() {
	const (
		username   = "test-user"
		token      = "test-token"
		walletName = "test-wallet-from"
		balance    = 245.67
		amount     = 123.45
	)
	var before = time.Now()
	var (
		t  = suite.T()
		r  = new(MockRepository)
		s  = NewService(r)
		w  = &Wallet{Name: walletName, Username: username}
		tx = &Transaction{
			ID:        1789,
			Operation: OpWithdraw,
			Info:      "withdraw funds from " + walletName,
			Token:     token,
			CreatedAt: before,
		}
		ch = &Change{
			Transaction: *tx,
			Wallet:      walletName,
			Amount:      -amount,
			CreatedAt:   before,
		}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
		timeFn = mock.MatchedBy(func(t time.Time) bool { return t.Sub(before) >= 0 })
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(w, nil)
	r.On("SumChanges", anyCtx, &ChangeFilters{WalletEQ: []string{walletName}}).Once().Return(balance, nil)
	r.On("AddTransaction",
		anyCtx,
		OpWithdraw,
		mock.MatchedBy(func(msg string) bool { return strings.Contains(msg, "withdraw of funds from "+walletName) }),
		token,
		mock.MatchedBy(func(t time.Time) bool { return t.Sub(before) >= 0 }),
	).Once().Return(tx, nil)
	r.On("AddChange", anyCtx, tx, walletName, -amount, timeFn).Once().Return(ch, nil)

	// --- business logic ---
	change, _ := s.Withdraw(ctx, username, token, walletName, amount)
	// ----------------------

	assert.NoError(t, resErr)
	assert.Equal(t, ch, change)

	r.AssertExpectations(t)
}

// -- Service.GetHistory --

type TestSuiteGetHistory struct {
	suite.Suite
}

func TestGetHistory(t *testing.T) {
	suite.Run(t, new(TestSuiteGetHistory))
}

func (suite *TestSuiteGetHistory) TestRepoFailsToGetWallet() {
	const (
		username   = "test-user"
		walletName = "test-wallet"
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		f *ChangeFilters
		e = errors.New("TEST ERROR")
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(nil, e)

	// --- business logic ---
	changes, _ := s.GetHistory(ctx, username, walletName, f)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, changes)

	r.AssertExpectations(t)
}

func (suite *TestSuiteGetHistory) TestNotFound() {
	const (
		username   = "test-user"
		walletName = "test-wallet"
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		f *ChangeFilters
		e = ErrNotFound{What: "wallet " + walletName}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(nil, e)

	// --- business logic ---
	changes, _ := s.GetHistory(ctx, username, walletName, f)
	// ----------------------

	require.Error(t, resErr)
	assert.EqualError(t, resErr, e.Error())
	assert.Nil(t, changes)

	r.AssertExpectations(t)
}

func (suite *TestSuiteGetHistory) TestDetectsWrongUser() {
	const (
		username   = "test-user"
		wrongUser  = "wrong-user"
		walletName = "test-wallet"
	)
	var (
		t = suite.T()
		r = new(MockRepository)
		s = NewService(r)
		f *ChangeFilters
		w = &Wallet{Name: walletName, Username: wrongUser}
		e = ErrNotFound{What: "wallet " + walletName}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(e)
	r.On("GetWallet", anyCtx, walletName).Once().Return(w, nil)

	// --- business logic ---
	changes, _ := s.GetHistory(ctx, username, walletName, f)
	// ----------------------

	require.Error(t, resErr)
	assert.EqualError(t, resErr, e.Error())
	assert.Nil(t, changes)

	r.AssertExpectations(t)
}

func (suite *TestSuiteGetHistory) TestRepoFailsToListChanges() {
	const (
		username   = "test-user"
		walletName = "test-wallet"
	)
	var (
		t  = suite.T()
		r  = new(MockRepository)
		s  = NewService(r)
		op = OpDeposit
		f1 = &ChangeFilters{WalletEQ: []string{"going-to-be-overridden"}, Operation: &op}
		f2 = &ChangeFilters{WalletEQ: []string{walletName}, Operation: &op}
		w  = &Wallet{Name: walletName, Username: username}
		e  = errors.New("TEST ERROR")
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(w, nil)
	r.On("ListChanges", anyCtx, f2).Once().Return(nil, e)

	// --- business logic ---
	changes, _ := s.GetHistory(ctx, username, walletName, f1)
	// ----------------------

	require.Error(t, resErr)
	assert.Contains(t, resErr.Error(), e.Error())
	assert.Nil(t, changes)

	r.AssertExpectations(t)
}

func (suite *TestSuiteGetHistory) TestRepoReturnsNoChanges() {
	const (
		username   = "test-user"
		walletName = "test-wallet"
	)
	var (
		t  = suite.T()
		r  = new(MockRepository)
		s  = NewService(r)
		f1 = &ChangeFilters{CreatedAtLE: time.Now().Add(-3 * time.Hour)}
		f2 = &ChangeFilters{WalletEQ: []string{walletName}, CreatedAtLE: f1.CreatedAtLE}
		w  = &Wallet{Name: walletName, Username: username}
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		innerFn func(Repository) error
		matchFn = mock.MatchedBy(func(fn func(Repository) error) bool {
			innerFn = fn
			return true
		})
		resErr   error
		runnerFn = func(_ mock.Arguments) {
			resErr = innerFn(r)
		}
	)

	r.On("Isolated", anyCtx, maxAttempts, matchFn).Once().Run(runnerFn).Return(nil)
	r.On("GetWallet", anyCtx, walletName).Once().Return(w, nil)
	r.On("ListChanges", anyCtx, f2).Once().Return([]*Change{}, nil)

	// --- business logic ---
	changes, _ := s.GetHistory(ctx, username, walletName, f1)
	// ----------------------

	assert.NoError(t, resErr)
	assert.NotNil(t, changes)
	assert.Empty(t, changes)

	r.AssertExpectations(t)
}
