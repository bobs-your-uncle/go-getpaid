package kv

import (
	"context"
	"errors"
	"fmt"
	"time"

	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/auth"
	"bitbucket.org/bobs-your-uncle/go-getpaid/pkg/kv"
)

const (
	keyPrefix               = "auth-kv/"
	keyPrefixPasswordHashes = keyPrefix + "password-hashes/"
	keyPrefixUsernames      = keyPrefix + "usernames/"
	keyPrefixTokens         = keyPrefix + "tokens/"
)

// repo is an implementation of the auth.Repository interface using a kv.KV storage
type repo struct {
	kv kv.AtomicKV
}

func NewRepository(kv kv.AtomicKV) auth.Repository {
	return &repo{
		kv: kv,
	}
}

func (r *repo) GetHashedPassword(ctx context.Context, username string) (string, error) {
	key := keyPrefixPasswordHashes + username
	val, err := r.kv.Get(key)
	if err != nil {
		if errors.As(err, &kv.ErrNotFound{}) {
			return "", auth.ErrNotFound{What: "username"}
		}
		return "", fmt.Errorf("failed to get a password hash from a KV: %w", err)
	}
	return val, nil
}

func (r *repo) SetHashedPassword(ctx context.Context, username, hashedPw string) error {
	key := kv.NewKey(keyPrefixPasswordHashes + username)
	_, err := r.kv.Set(key, hashedPw, kv.UpdateExisting)
	if err != nil {
		return fmt.Errorf("failed to set a password hash in a KV: %w", err)
	}
	return nil
}

func (r *repo) GetUsernameByToken(ctx context.Context, token string) (string, error) {
	key := keyPrefixUsernames + token
	val, err := r.kv.Get(key)
	if err != nil {
		if errors.As(err, &kv.ErrNotFound{}) {
			return "", auth.ErrNotFound{What: "token"}
		}
		return "", fmt.Errorf("failed to get a username from a KV: %w", err)
	}
	return val, nil
}

func (r *repo) GetTokenByUsername(ctx context.Context, username string) (string, error) {
	key := keyPrefixTokens + username
	val, err := r.kv.Get(key)
	if err != nil {
		if errors.As(err, &kv.ErrNotFound{}) {
			return "", auth.ErrNotFound{What: "username"}
		}
		return "", fmt.Errorf("failed to get a token from a KV: %w", err)
	}
	return val, nil
}

func (r *repo) AssociateToken(ctx context.Context, username, token string, ttl time.Duration) error {
	key := kv.NewKey(keyPrefixTokens + username).WithExpiration(ttl)
	_, err := r.kv.Set(key, token, kv.UpdateExisting)
	if err != nil {
		return fmt.Errorf("failed to set a token in a KV: %w", err)
	}

	// Warning: here is a chance of a race condition.
	//          both these operations have to be made atomically.
	// TODO

	key = kv.NewKey(keyPrefixUsernames + token).WithExpiration(ttl)
	_, err = r.kv.Set(key, username, kv.UpdateExisting)
	if err != nil {
		return fmt.Errorf("failed to set a username in a KV: %w", err)
	}

	return nil
}

func (r *repo) InvalidateToken(ctx context.Context, token string) error {
	key := keyPrefixUsernames + token
	username, err := r.kv.Get(key)
	if err != nil {
		if errors.As(err, &kv.ErrNotFound{}) {
			return nil
		}
		return fmt.Errorf("failed to get a username from a KV: %w", err)
	}

	_, err = r.kv.Delete(key, keyPrefixTokens + username)
	if err != nil {
		return fmt.Errorf("failed to delete a username/token from a KV: %w", err)
	}

	return nil
}
