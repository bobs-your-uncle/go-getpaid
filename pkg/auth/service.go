// contains a simple implementation of the auth.Service interface
package auth

import (
	"context"
	"errors"
	"fmt"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type service struct {
	repo Repository
}

// NewService creates a new auth.Service that uses an abstracted Repository
// to remember active tokens.
//
func NewService(repo Repository) Service {
	return &service{
		repo: repo,
	}
}

func (svc *service) CheckUsername(ctx context.Context, username string) (bool, error) {

	// check the user's password hash
	_, err := svc.repo.GetHashedPassword(ctx, username)
	if err != nil {
		if errors.As(err, &ErrNotFound{}) {
			// there is no such user
			return false, ErrNotAllowed{Reason: err.Error()}
		}
		return false, fmt.Errorf("failed to get a user password hash: %w", err)
	}

	return true, nil
}
func (svc *service) CheckUsernamePassword(ctx context.Context, username, password string) (bool, error) {

	// check the user's password hash
	expected, err := svc.repo.GetHashedPassword(ctx, username)
	if err != nil {
		if errors.As(err, &ErrNotFound{}) {
			// there is no such user
			return false, ErrNotAllowed{Reason: err.Error()}
		}
		return false, fmt.Errorf("failed to get a user password hash: %w", err)
	}

	err = bcrypt.CompareHashAndPassword([]byte(expected), []byte(password))
	if err != nil {
		return false, ErrNotAllowed{Reason: "password doesn't match"}
	}

	return true, nil
}

func (svc *service) CheckToken(ctx context.Context, token string) (bool, error) {

	_, err := svc.repo.GetUsernameByToken(ctx, token)
	if err != nil {
		if errors.As(err, &ErrNotFound{}) {
			// there is no such token or user
			return false, ErrNotAllowed{Reason: err.Error()}
		}
		return false, fmt.Errorf("failed to get a user by token: %w", err)
	}

	return true, nil
}

func (svc *service) RegisterUser(ctx context.Context, username, password string) error {

	// make sure the username is unknown
	_, err := svc.repo.GetHashedPassword(ctx, username)
	if err == nil {
		return fmt.Errorf("the username already exists")
	}
	if !errors.As(err, &ErrNotFound{}) {
		return fmt.Errorf("failed to lookup username: %w", err)
	}

	// store the password hash
	hashed, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return fmt.Errorf("failed to generate a hash: %w", err)
	}

	err = svc.repo.SetHashedPassword(ctx, username, string(hashed))
	if err != nil {
		return fmt.Errorf("failed to store a password hash: %w", err)
	}

	return nil
}

func (svc *service) AssociateToken(ctx context.Context, username, token string, ttl time.Duration) error {

	// check the username
	_, err := svc.repo.GetHashedPassword(ctx, username)
	if err != nil {
		if errors.As(err, &ErrNotFound{}) {
			// there is no such user
			return ErrNotAllowed{Reason: err.Error()}
		}
		return fmt.Errorf("failed to get a user password hash: %w", err)
	}

	// make sure the token is unknown
	_, err = svc.repo.GetUsernameByToken(ctx, token)
	if err == nil {
		return fmt.Errorf("the token already exists")
	}
	if !errors.As(err, &ErrNotFound{}) {
		return fmt.Errorf("failed to lookup token: %w", err)
	}

	err = svc.repo.AssociateToken(ctx, username, token, ttl)
	if err != nil {
		return fmt.Errorf("failed to associate a token: %w", err)
	}

	return nil
}

func (svc *service) InvalidateToken(ctx context.Context, token string) error {

	err := svc.repo.InvalidateToken(ctx, token)
	if err != nil {
		return fmt.Errorf("failed to invalidate a token: %w", err)
	}

	return nil
}

