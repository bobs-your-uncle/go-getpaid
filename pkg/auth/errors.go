package auth

import "fmt"

// ErrNotAllowed - an error that is returned when either a username/password don't match
// or a token is not valid
type ErrNotAllowed struct {
	Reason string
}

func (err ErrNotAllowed) Error() string {
	return err.Reason
}

// ErrNotFound - an error that is returned from a Repository when either a user
// or a token is not found
type ErrNotFound struct {
	What string
}

func (err ErrNotFound) Error() string {
	return fmt.Sprintf("%v not found", err.What)
}
