package auth

import (
	"context"
	"time"
)

// Checker - a checking part of the an abstract authentication Service
type Checker interface {
	// CheckUsername returns true if the username exists and an associated user is unrestricted
	CheckUsername(ctx context.Context, username string) (bool, error)

	// CheckUsernamePassword verifies the username and password match an existing
	// and unrestricted user
	CheckUsernamePassword(ctx context.Context, username, password string) (bool, error)

	// CheckToken verifies the given token is active and an associated user is unrestricted
	CheckToken(ctx context.Context, token string) (bool, error)
}

// Service - an abstract authentication service
type Service interface {
	Checker

	// RegisterUser checks and adds new user to the repository
	RegisterUser(ctx context.Context, username, password string) error

	// AssociateToken binds the given `token` with the `username` for the `ttl` duration
	AssociateToken(ctx context.Context, username, token string, ttl time.Duration) error

	// InvalidateToken marks the given token as invalid making sure any subsequent calls to
	// CheckToken would return false
	InvalidateToken(ctx context.Context, token string) error
}

type Repository interface {
	GetHashedPassword(ctx context.Context, username string) (string, error)
	SetHashedPassword(ctx context.Context, username, hashedPw string) error

	GetUsernameByToken(ctx context.Context, token string) (string, error)
	GetTokenByUsername(ctx context.Context, username string) (string, error)

	AssociateToken(ctx context.Context, username, token string, ttl time.Duration) error
	InvalidateToken(ctx context.Context, token string) error
}
