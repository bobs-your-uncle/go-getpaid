
CREATE TABLE wallets (
	id         bigserial PRIMARY KEY,
	name       varchar(128) NOT NULL,
	username   varchar(255) NOT NULL,
	created_at timestamp NOT NULL
);
CREATE UNIQUE INDEX idx_wallets_name ON wallets(name);
CREATE INDEX idx_wallets_username_at ON wallets(username, created_at);

CREATE TYPE operation AS ENUM ('transfer', 'deposit', 'withdraw');

CREATE TABLE transactions (
	id         bigserial PRIMARY KEY,
	op         operation NOT NULL,
	info       text NOT NULL,
	token      varchar(1024) NOT NULL,
	created_at timestamp NOT NULL
);
CREATE INDEX idx_transactions_at ON transactions(created_at);

CREATE TABLE changes (
	id         bigserial PRIMARY KEY,
	trans_id   bigint REFERENCES transactions(id),
	wallet_id  bigint REFERENCES wallets(id),
	amount     decimal NOT NULL,
	created_at timestamp NOT NULL
);
CREATE INDEX idx_changes_trans_id     ON changes(trans_id);
CREATE INDEX idx_changes_wallet_id_at ON changes(wallet_id, created_at);
CREATE INDEX idx_changes_at           ON changes(created_at);
