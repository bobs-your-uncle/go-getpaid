#!/bin/sh

set -e

[ -z "$TOKEN" ] && echo "\nIMPORTANT: TOKEN env var must be set to a correct JWT token (./signup.sh or ./auth.sh)\n" && exit 1

if [ -z "$1" ]; then
	echo "Usage: $0 <wallet_name> [query-params]\n\n"
	echo "Sends a request to get wallet history (wallet_name must exist).\n\n"
	echo "Example: $0 my-wallet '?fmt=json&from=2020-09-08T19:00:00Z&op=deposit'\n"
	exit 1
fi

curl "http://localhost/history/$1$2" \
	-H "Authorization: Bearer $TOKEN" \
    -D -
