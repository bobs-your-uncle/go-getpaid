#!/bin/sh

set -e

[ -z "$TOKEN" ] && echo "\nIMPORTANT: TOKEN env var must be set to a correct JWT token (./signup.sh or ./auth.sh)\n" && exit 1

[ -z "$1" ] && echo "Usage: $0 <wallet_name>\n\nSends a request to get wallet balance (wallet_name must exist).\n" && exit 1

curl "http://localhost/balance/$1" \
	-H "Authorization: Bearer $TOKEN" \
    -D -
