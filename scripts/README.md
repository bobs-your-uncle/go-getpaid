
This directory contains several scripts to play with the REST API expsed by `docker-compose up`.

The following assumes you have already started a test cluster with *docker-compose*.

Generally you start with `./signup.sh <username>` or `./auth.sh <username>`.
This should output a correct token.
Copy it and setup the environment like this: `export TOKEN="...token-data..."`
Now all other scripts would put the exported token in a *Authorization:* header.
Obviously different tokens should be played with in different terminals.

