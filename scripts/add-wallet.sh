#!/bin/sh

set -e

[ -z "$TOKEN" ] && echo "\nIMPORTANT: TOKEN env var must be set to a correct JWT token (./signup.sh or ./auth.sh)\n" && exit 1

[ -z "$1" ] && echo "Usage: $0 <wallet_name>\n\nSends a request to add new wallet (wallet_name must not exist).\n" && exit 1

curl -X POST "http://localhost/wallets/$1" \
	-H "Authorization: Bearer $TOKEN" \
    -D -
