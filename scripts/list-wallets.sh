#!/bin/sh

set -e

[ -z "$TOKEN" ] && echo "\nIMPORTANT: TOKEN env var must be set to a correct JWT token (./signup.sh or ./auth.sh)\n" && exit 1

curl http://localhost/wallets \
	-H "Authorization: Bearer $TOKEN" \
    -D -
