#!/bin/sh

set -e

[ -z "$TOKEN" ] && echo "\nIMPORTANT: TOKEN env var must be set to a correct JWT token (./signup.sh or ./auth.sh)\n" && exit 1

[[ ( -z "$1" ) || ( -z "$2" ) || ( -z "$3" ) ]] && echo "Usage: $0 <from_wallet> <to_wallet> <amount>\n\nSends a request to deposit funds to a wallet (wallet_name must exist).\n" && exit 1

curl -X POST "http://localhost/transfer" \
    -d "{\"from\":\"$1\", \"to\":\"$2\", \"amount\":$3}" \
    -H "Content-Type: application/json" \
	-H "Authorization: Bearer $TOKEN" \
    -D -
