#!/bin/sh

set -e

[ -z "$TOKEN" ] && echo "\nIMPORTANT: TOKEN env var must be set to a correct JWT token (./signup.sh or ./auth.sh)\n" && exit 1

[[ ( -z "$1" ) || ( -z "$2" ) ]] && echo "Usage: $0 <wallet_name> <amount>\n\nSends a request to withdraw funds from a wallet (wallet_name must exist).\n" && exit 1

curl -X POST "http://localhost/withdraw" \
    -d "{\"wallet\":\"$1\", \"amount\":$2}" \
    -H "Content-Type: application/json" \
	-H "Authorization: Bearer $TOKEN" \
    -D -
