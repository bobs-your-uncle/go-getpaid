#!/bin/sh

set -e

[ -z "$1" ] && echo "Usage: $0 <username>\n\nRegisters new user and outputs a session token (username must not exist).\n" && exit 1

curl http://localhost/signup \
    -d "{\"username\":\"$1\", \"password\":\"pw\"}" \
    -H "Content-Type: application/json" \
    -D -
