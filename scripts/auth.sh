#!/bin/sh

set -e

[ -z "$1" ] && echo "Usage: $0 <username>\n\nSends an auth request to retrieve a session token (username must exist).\n" && exit 1

curl http://localhost/auth \
    -d "{\"username\":\"$1\", \"password\":\"pw\"}" \
    -H "Content-Type: application/json" \
    -D -
