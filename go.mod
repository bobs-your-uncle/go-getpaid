module bitbucket.org/bobs-your-uncle/go-getpaid

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/doug-martin/goqu/v9 v9.9.0
	github.com/gomodule/redigo v1.8.2
	github.com/jackc/pgx/v4 v4.8.1
	github.com/labstack/echo/v4 v4.1.17
	github.com/labstack/gommon v0.3.0
	github.com/stretchr/testify v1.5.1
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0
)
